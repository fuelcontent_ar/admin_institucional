# Proyecto de AdecoAgro

### Paso para ejecutar el proyecto

1. Descargar el proyecto
2. Copiar el .env-example a .env
3. Crear una base de datos y luego agregar los parametros al .env
4. Comandos a usar:
    
    - composer update
    - php artisan key:generate
    - php artisan storage:link
    - php artisan migrate
    - php artisan db:seed
    - php artisan serve
    
Ya con esos parametros deberia de tener la vista del proyecto

Nota: si utilizas el comando de php artisan serve, agregar en el .env

APP_URL = http://127.0.0.1:8000
