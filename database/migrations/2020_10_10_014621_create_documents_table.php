<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->text("document_url");
            $table->text("image_url");
            $table->string("lang");
            $table->boolean('highlighted')->default(false);
            $table->unsignedBigInteger("document_type_id");
            $table->unsignedBigInteger("user_id");
            $table->foreign("document_type_id")->references("id")->on("document_types");
            $table->foreign("user_id")->references("id")->on("users");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
