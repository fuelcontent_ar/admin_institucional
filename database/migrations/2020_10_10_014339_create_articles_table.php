<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string("title", 100);
            $table->string("slug", 100);
            $table->text("description")->nullable();
            $table->text("header_image")->nullable();
            $table->text("card_image")->nullable();
            $table->string("lang");
            $table->json("body");
            $table->text("image_url");
            $table->boolean('published')->default(false);
            $table->boolean('highlighted')->default(false);
            $table->dateTime("published_at")->useCurrent();
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign("user_id")->references("id")->on("users");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
