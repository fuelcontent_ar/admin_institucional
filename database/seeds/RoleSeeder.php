<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ["name" => "admin"],
            ["name" => "editor"]
        ];

        foreach ($roles as $rol) {
            \Spatie\Permission\Models\Role::query()->updateOrCreate($rol, $rol);
        }
    }
}
