<?php

use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ["title" => "REPORTES DE SUSTENTABILIDAD", "image_url" => "/storage/documenttypes/sustentabilidad.svg"],
            ["title" => "COMPROMISOS", "image_url" => "/storage/documenttypes/compromisos.svg"],
            ["title" => "DOCUMENTOS", "image_url" => "/storage/documenttypes/documentos.svg"],
        ];
        foreach ($categories as $category) {
            $doctype = new \App\DocumentType([
                "title" => $category["title"],
                "image_url" => $category["image_url"],
                "user_id" => \App\User::query()->inRandomOrder()->first()->getAttribute("id")
            ]);
            $doctype->save();
        }
    }
}
