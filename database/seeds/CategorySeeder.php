<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ["name" => "GESTION AMBIENTAL", "active" => true],
            ["name" => "COMUNIDAD", "active" => true],
            ["name" => "SEGURIDAD", "active" => true],
            ["name" => "TRANSPARENCIA", "active" => true],
        ];
        foreach ($categories as $category) {
            \App\Category::query()->updateOrCreate(
                ["name" => $category["name"],],
                $category
            );
        }
    }
}
