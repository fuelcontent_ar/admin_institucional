<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        "title" => $faker->sentence,
        "category_id" => \App\Category::query()->inRandomOrder()->first()->getAttribute("id"),
        "user_id" => \App\User::query()->inRandomOrder()->first()->getAttribute("id"),
    ];
});
