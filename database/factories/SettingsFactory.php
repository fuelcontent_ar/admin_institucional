<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Setting;
use Faker\Generator as Faker;

$factory->define(Setting::class, function (Faker $faker) {
    return [
        "key" => $faker->word,
        "value" => json_encode([
            "name" => $faker->name,
            "date" => $faker->date(),
            "address" => $faker->address,
            "test" => $faker->words
        ]),
        "user_id" => \App\User::query()->find(1)->id
    ];
});
