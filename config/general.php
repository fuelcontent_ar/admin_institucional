<?php

return [
    "default-password" => 1234,
    "language" => [
        "es" => "Español",
        "en" => "Ingles",
        "pt" => "Portugues",
    ]
];
