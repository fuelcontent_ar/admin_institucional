<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

\Illuminate\Support\Facades\Auth::routes(["register" => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware("auth")->group(function () {
    Route::resource("/users", "UserController");
//    Route::resource("/blogs", "BlogController")->except(["edit", "create"]);
    Route::resource("/document-type", "DocumentTypeController")->except(["edit", "create"]);
    Route::post("/articles/uploadFile", "ArticleController@uploadFile");
    Route::post("/articles/deleteFile", "ArticleController@deleteFile");
    Route::resource("/articles", "ArticleController");
    Route::resource("/documents", "DocumentController")->except("show");
    Route::resource("/settings", "SettingController")->except("show");
});
