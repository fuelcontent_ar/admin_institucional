class Functions {

    /**
     * @param url
     * @param method
     * @param data
     * @param headers
     * @param placeholder
     */
    request(url, method = "GET", data = {}, headers = {}, placeholder = function(){}) {
        switch (method) {
            case "POST":
            case "PUT":
            case "PATCH":
            case "DELETE":
                headers['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
        }
        return $.ajax({
            headers: headers,
            url : url,
            data : data,
            type : method,
            beforeSend: placeholder,
            dataType : 'json',
        });
    }
}
