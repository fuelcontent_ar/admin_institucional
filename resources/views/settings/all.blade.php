@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('settings.all.title') }}
                        <a class="btn btn-success btn-sm float-right" href="{{route("settings.create")}}"
                           data-toggle="tooltip" data-placement="right" title="{{__("settings.save.title")}}">
                            <i class="fa fa-plus"></i> Nuevo
                        </a>
                    </div>
                    <div class="card-body">
                        <x-table :columns="$columns" :dataTablesColumns="$dataTablesColumns" :route="$route"
                                 :config="$config" name="settings"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script>
        var table;
        let functions = new Functions();
        $(function () {
            $("#alert").hide();
            $('[data-toggle="tooltip"]').tooltip()
        })

        /** start request delete title blogs **/
        function drop(settingId) {
            console.log(settingId)
            let response;
            let alert = new Alerts();
            alert.warning(
                "{{__("general.alert-type.warning")}}",
                "{{__("general.delete.message")}}",
                {yes: "{{__("general.buttons.yes")}}", no: "{{__("general.buttons.no")}}"},
                async function () {
                response = await functions.request('{{url("settings")}}/' + settingId, "DELETE");
                if (response) {
                    swal("{{__("general.delete.title")}}", response, "success");
                    table.ajax.reload();
                }
            });
        }

        /** end request delete blog **/
        /** start request show blog **/
        function show() {
            clearErrors();
            $("#titleModal").html("{{__("settings.save.title")}}")
            $("#edit").val("");
            $("#form").trigger("reset");
            $("#modal").modal({keyboard: false})
        }
        /** end request show blog **/

        /** start request create title blogs **/
        function create() {
            clearErrors();
            let data = {
                title: $("#title").val(),
                category:  $("#category").val(),
            };
            functions.request('{{route("settings.store")}}', "POST", data, {}, function () {
                $("#text-create").hide();
                $("#text-load").show();
                $("#load").show();
                $("#primary").attr("disabled", "disabled");
            }).then(() => {
                $("#text-create").show();
                $("#text-load").hide();
                $("#primary").removeAttr("disabled");
                $("#load").hide();
                $("#form").trigger("reset");
                table.ajax.reload();
                $("#modal").modal("hide");
                swal("Created!", "{{__("settings.save.message")}}", "success");
            }).catch(err => {
                $("#text-create").show();
                $("#primary").removeAttr("disabled");
                $("#text-load").hide();
                $("#load").hide();
                for (let error in err.responseJSON.errors) {
                    $("#error_"+error).html(err.responseJSON.errors[error]).show();
                }
            });
        }
        /** end request create blog **/

        /** start request get title blogs **/
        async function get(blogId) {
            let response;
            clearErrors();
            response = await functions.request('{{url("/settings")}}/'+blogId, "GET");
            if (response) {
                $("#titleModal").html("{{__("settings.edit.title")}}")
                $("#edit").val(blogId);
                $("#title").val(response.data.title);
                $("#category").val(response.data.category_id).change();
                $("#modal").modal("show");
            }
        }
        /** end request get blog **/

        /** start request edit title blogs **/
        function edit() {
            clearErrors();
            let blogId = $("#edit").val();
            let data = {
                title: $("#title").val(),
                category:  $("#category").val(),
            };
            functions.request('{{url("/settings")}}/'+blogId, "PATCH", data, {}, function () {
                $("#text-create").hide();
                $("#text-load").show();
                $("#load").show();
                $("#primary").attr("disabled", "disabled");
            }).then(() => {
                $("#text-create").show();
                $("#primary").removeAttr("disabled");
                $("#text-load").hide();
                $("#load").hide();
                $("#form").trigger("reset");
                table.ajax.reload();
                $("#modal").modal("hide");
                swal("Updated!", "{{__("settings.edit.message")}}", "success");
            }).catch(err => {
                $("#text-create").show();
                $("#primary").removeAttr("disabled");
                $("#text-load").hide();
                $("#load").hide();
                for (let error in err.responseJSON.errors) {
                    $("#error_"+error).html(err.responseJSON.errors[error]).show();
                }
            });
        }
        /** end request edit blog **/
        function execute() {
            if ($("#edit").val() === "") {
                return create();
            }
            return edit();
        }

        function clearErrors() {
            $("#error_title").html("").hide();
            $("#error_category").html("").hide();
        }
    </script>
@endsection


