<div>
    <a class="btn btn-warning btn-sm" href="{{route("settings.edit", $model->getAttribute("id"))}}"
       data-toggle="tooltip" data-placement="right" title="{{__("settings.all.tooltip-edit")}}">
        <i class="fa fa-pencil"></i>
    </a>
    <button type="button" class="btn btn-danger btn-sm" onclick="drop({{$model->getAttribute("id")}})"
            data-toggle="tooltip" data-placement="right" title="{{__("settings.all.tooltip-destroy")}}">
        <i class="fa fa-trash"></i>
    </button>
</div>
