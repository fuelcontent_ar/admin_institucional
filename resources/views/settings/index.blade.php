@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-10">
                <span id="message"></span>
                <div class="card">
                    <div class="card-header">{{ __(sprintf('settings.%s.title', $title)) }}</div>
                    <div class="card-body">
                        @if ($errors->any() || session()->has("message"))
                            @php
                                $type = session()->has("message") ? "success" : "danger";
                                $message = session()->has("message") ? session()->get("message") : __('validation.message');
                            @endphp
                            <x-alert :message="$message" :type="$type" :show="true"/>
                        @endif
                        <form method="POST"
                              action="{{isset($data) ? route("settings.update", $data["id"]) : route("settings.store")}}"
                              id="form"
                        >
                            @isset($data)
                                @method("PUT")
                            @endisset
                            @csrf
                            <div class="form-group">
                                <label for="title">{{__("settings.labels.key")}}</label>
                                <input type="text"
                                       class="form-control"
                                       id="key"
                                       name="key"
                                       placeholder="{{__("settings.labels.key")}}"
                                       value="{{ isset($data) ? $data["key"] : old("key") }}"
                                />
                            </div>
                            <input type="hidden" id="value" name="value">
                            <div id="output"></div>

                            <button type="submit"
                                    class="btn btn-primary" id="send">{{__(sprintf('settings.%s.button', $title))}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script>
        $(function () {
            let data = {!! isset($data) ? json_encode($data["value"]) : json_encode("{}") !!};
            let json = new JSONedtr(  data, '#output' );
            $("#file").change(function () {
                if ($(this).val() !== "") {
                    $(".custom-file-label").text($(this).val());
                }
            })
            $("#send").click(function (e) {
                console.log("click", e);
                if ($("#key").val() !== "" && Object.keys(json.getData()).length !== 0) {
                    $("#value").val(JSON.stringify(json.getData()))
                    //$("#form").submit();
                    console.log($("#form"));
                } else {
                    swal("Error", "{{__("general.validate.error")}}", "error");
                    e.preventDefault();
                    return false;
                }
            })
        })
    </script>
@endsection


