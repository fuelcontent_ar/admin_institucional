@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('document_type.all.title') }}
                        <a class="btn btn-success btn-sm float-right" onclick="show()"
                           data-toggle="tooltip" data-placement="right" title="{{__("document_type.save.title")}}">
                            <i class="fa fa-plus"></i> Nuevo
                        </a>
                    </div>
                    <div class="card-body">
                        <x-table :columns="$columns" :dataTablesColumns="$dataTablesColumns" :route="$route"
                                 :config="$config" name="document_type"/>
                        <x-modal :title="__('documents.save.title')" :cancel="true" :save="true" size="modal-lg">
                            <form id="form">
                            <input type="hidden" id="edit">
                            <div class="form-group row">
                                <label for="title" class="col-sm-3 col-form-label">{{__("document_type.labels.title")}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="title"
                                           placeholder="{{__("document_type.labels.title")}}">
                                    <span class="error" id="error_title"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="main_image" class="col-sm-3 col-form-label">{{__("articles.labels.image")}}</label>
                                <div class="col-sm-9">
                                    <div class="file-loading">
                                        <input id="main_image" name="image" type="file">
                                    </div>
                                </div>
                            </div>
                            </form>
                        </x-modal>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script>
        var table;
        let functions = new Functions();
        $(function () {
            $("#alert").hide();
            $('[data-toggle="tooltip"]').tooltip()
        })

        /** start request delete title document_type **/
        function drop(documentTypeId) {
            let alert = new Alerts();
            alert.warning(
                "{{__("general.alert-type.warning")}}",
                "{{__("general.delete.message")}}",
                {yes: "{{__("general.buttons.yes")}}", no: "{{__("general.buttons.no")}}"},
                function () {
                    functions.request('{{url("document-type")}}/' + documentTypeId, "DELETE")
                        .then(function (response){
                        swal("{{__("general.delete.title")}}", response, "success");
                        table.ajax.reload();
                        closeAlert();
                    }).catch(function (err) {
                        if (err.status === 403) {
                            alertError(err);
                        } else {
                            swal("Error", '{{__("general.error_server")}}', "error");
                            closeAlert();
                        }
                    })

            });
        }

        /** end request delete blog **/
        /** start request show blog **/
        function show() {
            clearErrors();
            $("#titleModal").html("{{__("document_type.save.title")}}")
            $("#edit").val("");
            $("#form").trigger("reset");
            $("#modal").modal({keyboard: false})
        }
        /** end request show blog **/

        /** start request create title documents **/
        function create() {
            clearErrors();
            let data = {
                title: $("#title").val(),
                image: $("#image").val(),
            };
            functions.request('{{route("document-type.store")}}', "POST", data, {}, function () {
                $("#text-create").hide();
                $("#text-load").show();
                $("#load").show();
                $("#primary").attr("disabled", "disabled");
            }).then(function () {
                $("#text-create").show();
                $("#text-load").hide();
                $("#primary").removeAttr("disabled");
                $("#load").hide();
                $("#form").trigger("reset");
                table.ajax.reload();
                $("#modal").modal("hide");
                swal("Created!", "{{__("document_type.save.message")}}", "success");
            }).catch(function (err) {
                if (err.status === 422) {
                    $("#text-create").show();
                    $("#primary").removeAttr("disabled");
                    $("#text-load").hide();
                    $("#load").hide();
                    for (let error in err.responseJSON.errors) {
                        $("#error_"+error).html(err.responseJSON.errors[error]).show();
                    }
                } else{
                    swal("Error", '{{__("general.error_server")}}', "error");
                    closeAlert();
                }
            });
        }
        /** end request create blog **/

        /** start request get title documents **/
        function get(documentTypeId) {
            clearErrors();
            functions.request('{{url("/document-type")}}/'+documentTypeId, "GET")
                .then(function (response) {
                $("#titleModal").html("{{__("document_type.edit.title")}}")
                $("#edit").val(documentTypeId);
                $("#title").val(response.data.title);
                $("#modal").modal("show");
            }).catch(function (err) {
                if (err.status === 403) {
                    alertError(err);
                } else {
                    swal("Error", '{{__("general.error_server")}}', "error");
                    closeAlert();
                }
            });
        }
        /** end request get blog **/

        /** start request edit title documents **/
        function edit() {
            clearErrors();
            let documentTypeId = $("#edit").val();
            let data = {
                title: $("#title").val(),
                category:  $("#category").val(),
            };
            functions.request('{{url("/document-type")}}/'+documentTypeId, "PATCH", data, {}, function () {
                $("#text-create").hide();
                $("#text-load").show();
                $("#load").show();
                $("#primary").attr("disabled", "disabled");
            }).then(function () {
                $("#text-create").show();
                $("#primary").removeAttr("disabled");
                $("#text-load").hide();
                $("#load").hide();
                $("#form").trigger("reset");
                table.ajax.reload();
                $("#modal").modal("hide");
                swal("Updated!", "{{__("document_type.edit.message")}}", "success");
            }).catch(function (err) {
                if (err.status === 403) {
                    alertError(err);
                } else if (err.status === 422){
                    $("#text-create").show();
                    $("#primary").removeAttr("disabled");
                    $("#text-load").hide();
                    $("#load").hide();
                    for (let error in err.responseJSON.errors) {
                        $("#error_"+error).html(err.responseJSON.errors[error]).show();
                    }
                } else {
                    swal("Error", '{{__("general.error_server")}}', "error");
                    closeAlert();
                }
            });
        }
        /** end request edit blog **/
        function execute() {
            if ($("#edit").val() === "") {
                return create();
            }
            return edit();
        }

        function clearErrors() {
            $("#error_title").html("").hide();
        }

        function alertError(error) {
            swal("Error", error.responseJSON.message, "error");
            closeAlert();
        }

        function closeAlert() {
            setTimeout(function () {
                swal.close();
            }, 5000);
        }

        $(document).ready(function () {
            $("#main_image").fileinput({
                @if(isset($data) || old("image"))
                    initialPreview: "{{ \Illuminate\Support\Facades\Storage::disk("public")->url("articles/".$data["image_url"]) }}",
                    initialPreviewAsData: true,
                    deleteUrl: '{{url("articles/deleteFile")}}',
                    initialPreviewConfig:[
                        {
                            key: "{{ $data["image_url"] }}",
                        },
                    ],
                    ajaxDeleteSettings: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    },
                @endif
                overwriteInitial: false,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                browseIcon: " <i class='fa fa-folder-open-o'></i>",
                browseLabel: '',
                removeLabel: '',
                removeIcon: '<i class="fa fa-trash"></i>',
                uploadUrl: '{{url("articles/uploadFile")}}',
                required: true,
                removeTitle: 'Cancel or reset changes',
                elErrorContainer: '#kv-avatar-errors-2',
                msgErrorClass: 'alert alert-block alert-danger',
                defaultPreviewContent: '<img src="{{asset("images/file-upload-image-icon.png")}}" class="image" alt="Subir imagen">',
                layoutTemplates: {main2: '{preview} {remove} {browse}'},
                allowedFileExtensions: ["jpg", "png", "gif", "jpeg",],
                ajaxSettings: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                previewZoomButtonIcons: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>',
                    toggleheader: '<i class="fa fa-arrows-h"></i>',
                    fullscreen: '<i class="fa fa-arrows-alt"></i>',
                    borderless: '<i class="fa fa-arrows"></i>',
                    close: '<i class="fa fa-times"></i>'
                },
                fileActionSettings: {
                    showUpload: false,
                    showRemove: false,
                    zoomIcon: "<i class='fa fa-search'></i>",
                    zoomTitle: "Ver imagen",
                    removeIcon: "<i class='fa fa-trash'></i>",
                    removeTitle: "Quitar imagen"
                }
            });
        });
    </script>
@endsection


