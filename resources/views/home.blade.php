@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('home.dashboard') }} - {{ __('home.welcome') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>Estos son los recursos disponibles:</p>
                    <div class="row mt-2">
                        <div class="col-12 col-sm-6">
                            <a href="{{route("articles.index")}}" class="btn btn-primary">Artículos Novedades</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
