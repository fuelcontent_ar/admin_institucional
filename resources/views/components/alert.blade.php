<div class="alert alert-{{$type}} @empty(!$style) col-4 col-sm-4 col-xs-4 position-absolute @endempty"
     id="alert"
     @empty(!$style) style="{{$style}}" @endempty>
    @if($errors->any())
        <p>{{$message}}</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @else
        <p id="message">{{$message}}</p>
    @endif
</div>
@section("scripts")
    @parent
    <script>

        let duration = {!! $duration !!};

        $(function () {
            @if($show)
            setTimeout(function () {
                $("#alert").fadeOut("slow");
            }, duration)
            @else
            $("#alert").hide();
            @endif
        })

    </script>
@endsection
