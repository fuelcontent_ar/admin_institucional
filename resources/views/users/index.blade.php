@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-6">
                <span id="message"></span>
                <div class="card">
                    <div class="card-header">{{ __(sprintf('users.%s.title', $title)) }}</div>
                    <div class="card-body">
                        @if ($errors->any() || session()->has("message"))
                            @php
                                $type = session()->has("message") ? "success" : "danger";
                                $message = session()->has("message") ? session()->get("message") : __('validation.message');
                            @endphp
                            <x-alert :message="$message" :type="$type" :show="true"/>
                        @endif
                        <form method="POST"
                              action="{{isset($data) ? route("users.update", $data["id"]) : route("users.store")}}">
                            @isset($data)
                                @method("PUT")
                            @endisset
                            @csrf
                            <div class="form-group">
                                <label for="name">{{__("users.labels.name")}}</label>
                                <input type="text"
                                       class="form-control"
                                       id="name"
                                       name="name"
                                       placeholder="{{__("users.labels.name")}}"
                                       value="{{ isset($data) ? $data["name"] : old("name") }}"
                                />
                            </div>
                            <div class="form-group">
                                <label for="email">{{__("users.labels.email")}}</label>
                                <input type="email"
                                       class="form-control"
                                       name="email"
                                       id="email"
                                       placeholder="{{__("users.labels.email")}}"
                                       value="{{ isset($data) ? $data["email"] : old("email") }}"
                                />
                            </div>

                            <div class="form-group">
                                <label for="roles">Roles</label>
                                <select class="form-control" name="roles" id="roles">
                                    <option @if(!(isset($data) && !$data["rol"])) selected @endif>-- Seleccione --
                                    </option>
                                    @foreach($roles as $rol)
                                        <option
                                            value="{{$rol->name}}"
                                            @if((isset($data) && $data["rol"] === $rol->getAttribute("name"))) selected @endif
                                        >
                                            {{\Illuminate\Support\Str::title($rol->getAttribute("name"))}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group custom-control custom-switch">
                                <input type="checkbox"
                                       class="custom-control-input"
                                       name="active"
                                       id="active"
                                       @if((isset($data) && $data["active"])) checked
                                       @elseif(old("active")) checked @endif
                                />
                                <label class="custom-control-label" for="active">{{__("users.labels.active")}}</label>
                            </div>

                            <button type="submit"
                                    class="btn btn-primary">{{__(sprintf('users.%s.button', $title))}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")

@endsection


