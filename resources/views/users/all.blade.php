@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('users.all.title') }}
                        <a href="{{route("users.create")}}" class="btn btn-success btn-sm float-right"
                           data-toggle="tooltip" data-placement="right" title="{{__("users.all.tooltip-create")}}">
                            <i class="fa fa-plus"></i> Nuevo
                        </a>
                    </div>
                    <div class="card-body">
                        <x-alert type="success" vertical="top" horizontal="right"/>
                        <x-table :columns="$columns" :dataTablesColumns="$dataTablesColumns" :route="$route"
                                 :config="$config" name="users"/>
                        <x-modal :title="__('users.all.modal-title')" :close="true">
                            <div>
                                <div class="form-group row">
                                    <label for="staticName" class="col-sm-4 col-form-label"><b>Name</b></label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="staticName">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 col-form-label"><b>Email</b></label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="staticEmail">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticRol" class="col-sm-4 col-form-label"><b>Last Login</b></label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="staticRol">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticActive" class="col-sm-4 col-form-label"><b>Active</b></label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="staticActive">
                                    </div>
                                </div>
                            </div>
                        </x-modal>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script>
        var table;
        let functions = new Functions();
        $(function () {
            $("#alert").hide();
            $('[data-toggle="tooltip"]').tooltip()
        })

        /** start request active user **/
        async function active(userId) {
            let request = await functions.request(
                '{{url("users")}}/' + userId,
                'PATCH',
                {active: $("#active-" + userId).is(":checked") ? "on" : "off"}
            )
            if (request.data) {
                let message;
                if ($("#active-" + userId).is(":checked")) {
                    message = "{{__("users.all.enabled")}}";
                } else {
                    message = "{{__("users.all.disabled")}}";
                }

                $("#message").html(message);
                $("#alert").slideDown();
                setTimeout(function () {
                    $("#alert").fadeOut("slow");
                }, 3000)
            }
        }

        /** end request active user **/
        /** start request delete user **/
        function drop(userId) {
            let response;
            let alert = new Alerts();
            alert.warning(
                "{{__("general.alert-type.warning")}}",
                "{{__("general.delete.message")}}",
                {yes: "{{__("general.buttons.yes")}}", no: "{{__("general.buttons.no")}}"},
                async function () {
                response = await functions.request('{{url("users")}}/' + userId, "DELETE");
                if (response) {
                    swal("{{__("general.delete.title")}}", response, "success");
                    table.ajax.reload();
                }
            });
        }

        /** end request delete user **/
        /** start request show user **/
        async function show(userId) {
            let request = await functions.request(
                '{{url("users")}}/' + userId,
                'GET'
            );

            if (request.data) {
                $("#staticName").val(request.data.name);
                $("#staticEmail").val(request.data.email);
                $("#staticActive").val(request.data.active);
                $("#staticRol").val(request.data.rol);

                $("#modal").modal({keyboard: false});
            }
        }

        /** end request show user **/
    </script>
@endsection


