<div>
    <button type="button" class="btn btn-info btn-sm" onclick="show({{$model->getAttribute("id")}})"
            data-toggle="tooltip" data-placement="right" title="{{__("users.all.tooltip-show")}}">
        <i class="fa fa-eye"></i>
    </button>
    @if(!$model->isAdmin())
        <a href="{{route("users.edit", $model->getAttribute("id"))}}" class="btn btn-warning btn-sm"
           data-toggle="tooltip" data-placement="right" title="{{__("users.all.tooltip-edit")}}">
            <i class="fa fa-pencil"></i>
        </a>
        <button type="button" class="btn btn-danger btn-sm" onclick="drop({{$model->getAttribute("id")}})"
                data-toggle="tooltip" data-placement="right" title="{{__("users.all.tooltip-destroy")}}">
            <i class="fa fa-trash"></i>
        </button>
    @endif
</div>
