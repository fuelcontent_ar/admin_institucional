@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('documents.all.title') }}
                        <a class="btn btn-success btn-sm float-right" href="{{route("documents.create")}}"
                           data-toggle="tooltip" data-placement="right" title="{{__("documents.save.title")}}">
                            <i class="fa fa-plus"></i> Nuevo
                        </a>
                    </div>
                    <div class="card-body">
                        <x-table :columns="$columns" :dataTablesColumns="$dataTablesColumns" :route="$route"
                                 :config="$config" name="documents"/>
                        <x-modal :title="__('documents.save.title')" :cancel="true" :save="true" size="modal-lg">
                            <form id="form">
                            <input type="hidden" id="edit">
                            <div class="form-group row">
                                <label for="title" class="col-sm-3 col-form-label">{{__("documents.labels.title")}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="title"
                                           placeholder="{{__("documents.labels.title")}}">
                                    <span class="error" id="error_title"></span>
                                </div>
                            </div>
                            </form>
                        </x-modal>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script>
        let functions = new Functions();
        $(function () {
            $("#alert").hide();
            $('[data-toggle="tooltip"]').tooltip()
        })

        /** start request delete title documents **/
        function drop(documentTypeId) {
            let response;
            let alert = new Alerts();
            alert.warning(
                "{{__("general.alert-type.warning")}}",
                "{{__("general.delete.message")}}",
                {yes: "{{__("general.buttons.yes")}}", no: "{{__("general.buttons.no")}}"},
                function () {
                response = functions.request('{{url("documents")}}/' + documentTypeId, "DELETE")
                    .then(function (response) {
                        swal("{{__("general.delete.title")}}", response, "success");
                        table.ajax.reload();
                    }).catch(function (err) {
                        if (err.status === 403) {
                            alertError(err);
                        } else {
                            swal("Error", '{{__("general.error_server")}}', "error");
                            closeAlert();
                        }
                    });
            });
        }

        /** end request delete blog **/

        /** start request get title documents **/
        async function get(documentTypeId) {
            let response;
            clearErrors();
            response = await functions.request('{{url("/documents")}}/'+documentTypeId, "GET");
            if (response) {
                $("#titleModal").html("{{__("documents.edit.title")}}")
                $("#edit").val(documentTypeId);
                $("#title").val(response.data.title);
                $("#modal").modal("show");
            }
        }
        /** end request get blog **/

        function alertError(error) {
            swal("Error", error.responseJSON.message, "error");
            closeAlert();
        }

        function closeAlert() {
            setTimeout(function () {
                swal.close();
            }, 5000);
        }
    </script>
@endsection


