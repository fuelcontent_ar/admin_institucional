<div>
    <a class="btn btn-warning btn-sm" href="{{route("documents.edit", $model->getAttribute("id"))}}"
       data-toggle="tooltip" data-placement="right" title="{{__("document_type.all.tooltip-edit")}}">
        <i class="fa fa-pencil"></i>
    </a>
    <button type="button" class="btn btn-danger btn-sm" onclick="drop({{$model->getAttribute("id")}})"
            data-toggle="tooltip" data-placement="right" title="{{__("document_type.all.tooltip-destroy")}}">
        <i class="fa fa-trash"></i>
    </button>
</div>
