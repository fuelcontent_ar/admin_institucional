@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">
                <span id="message"></span>
                <div class="card">
                    <div class="card-header">{{ __(sprintf('documents.%s.title', $title)) }}</div>
                    <div class="card-body">
                        @if ($errors->any() || session()->has("message"))
                            @php
                                $type = session()->has("message") ? "success" : "danger";
                                $message = session()->has("message") ? session()->get("message") : __('validation.message');
                            @endphp
                            <x-alert :message="$message" :type="$type" :show="true"/>
                        @endif
                        <form method="POST" enctype="multipart/form-data"
                              action="{{isset($data) ? route("documents.update", $data["id"]) : route("documents.store")}}">
                            @isset($data)
                                @method("PUT")
                                <input type="hidden" name="document_url" value="{{$data["document_url"]}}">
                                <input type="hidden" name="image_url" value="{{$data["image_url"]}}">
                            @endisset
                            @csrf
                            <div class="form-group">
                                <label for="title">{{__("documents.labels.title")}}</label>
                                <input type="text"
                                       class="form-control"
                                       id="title"
                                       name="title"
                                       placeholder="{{__("documents.labels.title")}}"
                                       value="{{ isset($data) ? $data["title"] : old("title") }}"
                                />
                            </div>

                            <div class="form-group">
                                <label for="main_image">{{__("articles.labels.image")}}</label>
                                <div>
                                    <div class="file-loading">
                                        <input id="main_image" name="image" type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="document_type">{{__("documents.labels.document_type")}}</label>
                                <select class="form-control" name="document_type_id" id="document_type_id">
                                    <option @if(!(isset($data) && !$data["document_type"])) selected @endif>--
                                        Seleccione --
                                    </option>
                                    @foreach($categories as $category)
                                        <option
                                            value="{{$category->getAttribute("id")}}"
                                            @if((isset($data) && $data["document_type"]->getAttribute("id") === $category->getAttribute("id"))) selected @endif
                                        >
                                            {{\Illuminate\Support\Str::title($category->getAttribute("title"))}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="custom-control custom-switch mt-4 mb-3" data-placement="right">
                                <input type="checkbox"
                                       class="custom-control-input"
                                       id="highlighted"
                                       name="highlighted"
                                       @if((isset($data) && $data["highlighted"] === "Si" || old("highlighted"))) checked @endif
                                >
                                <label class="custom-control-label" for="highlighted">
                                    {{__("articles.highlight")}}
                                </label>
                            </div>

                            <div class="form-group">
                                <label for="lang" class="col-form-label">{{__("articles.labels.lang")}}</label>
                                <select class="form-control" name="lang" id="lang" required>
                                    <option @if(!(isset($data) && !$data["lang"] || !empty(old("lang")))) selected @endif>--
                                        Seleccione --
                                    </option>

                                    @foreach($language as $key => $lang)
                                        <option
                                            value="{{$key}}"
                                            @if((isset($data) && $data["lang"] === $lang || old("lang") === $key)) selected @endif
                                        >
                                            {{\Illuminate\Support\Str::title($lang)}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="title">{{__("documents.labels.file")}}</label>
                                <div class="custom-file">
                                    <input
                                        type="file"
                                        class="custom-file-input"
                                        id="file"
                                        name="file"
                                        accept="application/pdf"
                                    >
                                    <label class="custom-file-label"
                                           for="file">{{__("documents.labels.input-file")}}</label>
                                </div>
                            </div>

                            <button type="submit"
                                    class="btn btn-primary">{{__(sprintf('documents.%s.button', $title))}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script>
        $(function () {
            @if(isset($data) && $data["document_url"])
            $(".custom-file-label").text("{{$data["document_url"]}}")
            @endif
            $("#file").change(function () {
                if ($(this).val() !== "") {
                    let fileName = $(this).val().split("\\");
                    $(".custom-file-label").text(fileName[fileName.length - 1]);
                }
            })
        })
        $(document).ready(function () {
            $("#main_image").fileinput({
                @if(isset($data) || old("image"))
                    initialPreview: "{{ \Illuminate\Support\Facades\Storage::disk("public")->url("articles/".$data["image_url"]) }}",
                    initialPreviewAsData: true,
                    deleteUrl: '{{url("articles/deleteFile")}}',
                    initialPreviewConfig:[
                        {
                            key: "{{ $data["image_url"] }}",
                        },
                    ],
                    ajaxDeleteSettings: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    },
                @endif
                overwriteInitial: false,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                browseIcon: " <i class='fa fa-folder-open-o'></i>",
                browseLabel: '',
                removeLabel: '',
                removeIcon: '<i class="fa fa-trash"></i>',
                uploadUrl: '{{url("articles/uploadFile")}}',
                required: true,
                removeTitle: 'Cancel or reset changes',
                elErrorContainer: '#kv-avatar-errors-2',
                msgErrorClass: 'alert alert-block alert-danger',
                defaultPreviewContent: '<img src="{{asset("images/file-upload-image-icon.png")}}" class="image" alt="Subir imagen">',
                layoutTemplates: {main2: '{preview} {remove} {browse}'},
                allowedFileExtensions: ["jpg", "png", "gif", "jpeg",],
                ajaxSettings: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                previewZoomButtonIcons: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>',
                    toggleheader: '<i class="fa fa-arrows-h"></i>',
                    fullscreen: '<i class="fa fa-arrows-alt"></i>',
                    borderless: '<i class="fa fa-arrows"></i>',
                    close: '<i class="fa fa-times"></i>'
                },
                fileActionSettings: {
                    showUpload: false,
                    showRemove: false,
                    zoomIcon: "<i class='fa fa-search'></i>",
                    zoomTitle: "Ver imagen",
                    removeIcon: "<i class='fa fa-trash'></i>",
                    removeTitle: "Quitar imagen"
                }
            });
        });
    </script>
@endsection


