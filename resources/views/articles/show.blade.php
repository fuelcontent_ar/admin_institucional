@extends('layouts.app')

@section('content')
    <div class="container">
        <form
            action="{{isset($data) ? route("articles.update", $data["id"]) : route("articles.store")}}"
            method="POST"
            id="form"
            enctype="multipart/form-data"
        >
            @isset($data)
                @method("PUT")
                <input type="hidden" name="image_url" value="{{$data["image_url"]}}">
                <input type="hidden" name="id" value="{{$data["id"]}}">
            @endisset
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <span id="message"></span>
                    <div class="card">
                        <div class="card-header">{{ __(sprintf('articles.%s.title', $title)) }}</div>
                        <div class="card-body">
                            @if ($errors->any() || session()->has("message"))
                                @php
                                    $type = session()->has("message") ? "success" : "danger";
                                    $message = session()->has("message") ? session()->get("message") : __('validation.message');
                                @endphp
                                <x-alert :message="$message" :type="$type" :show="true" duration="100000"/>
                            @endif
                            <div class="form-group">
                                <label for="title">{{__("articles.labels.title")}}</label>
                                <input type="text"
                                       class="form-control"
                                       id="title"
                                       name="title"
                                       required
                                       placeholder="{{__("articles.labels.title")}}"
                                       value="{{ isset($data) ? $data["title"] : old("title") }}"
                                />
                            </div>
                            <div class="form-group">
                                <label for="slug">{{__("articles.labels.slug")}}</label>
                                <input type="text"
                                       class="form-control"
                                       id="slug"
                                       name="slug"
                                       required
                                       placeholder="{{__("articles.labels.slug")}}"
                                       value="{{ isset($data) ? $data["slug"] : old("slug") }}"
                                />
                            </div>
                            <div class="form-group">
                                <label for="description">{{__("articles.labels.description")}}</label>
                                <textarea
                                    name="description"
                                    class="form-control"
                                    id="description"
                                    cols="30"
                                    rows="3"
                                    placeholder="{{__("articles.labels.description")}}"
                                >{{ isset($data) ? $data["description"] : old("description") }}</textarea>
                            </div>
                            
                            <div class="form-group ">
                                <label for="card_image_file">Imagen interna artículo:</label>
                                <div >
                                    <input id="card_image_file" name="card_image_file" type="file" >
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="header_image_file">Imagen header artículo:</label>
                                <div  >
                                    <input id="header_image_file" name="header_image_file" type="file" >
                                </div>
                            </div>
                            
                            <label for="editorjs" class="col-form-label">{{__("articles.labels.body")}}</label>
                            <div class="form-group">
                                <div id="editorjs" style="border:1px solid #ced4da; padding: 1%;"></div>
                            </div>
                            <input type="hidden" id="articleBody" name="body" value="{{ isset($data) ? $data["body"] : old("body", json_encode([])) }}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">{{__("articles.option")}}</div>
                        <div class="card-body">
                            <div class="custom-control custom-switch" data-placement="right">
                                <input type="checkbox"
                                       class="custom-control-input"
                                       id="publish"
                                       name="published"
                                       @if((isset($data) && $data["published"] === "Si" || old("published"))) checked @endif
                                >
                                <label class="custom-control-label" for="publish">
                                    {{__("articles.enable-disable")}}
                                </label>
                            </div>
                            <br>
                            <div class="form-group" data-provide="datepicker">
                                <label for="title">{{__("articles.labels.published_at")}}</label>
                                <input
                                    size="16"
                                    type="text"
                                    class="form-control"
                                    id="datetime"
                                    name="published_at"
                                    placeholder="{{__("articles.labels.published_at")}}"
                                    value="{{ isset($data) ? $data["published_at"] : old("published_at") }}"
                                >
                            </div>
                            <div class="custom-control custom-switch" data-placement="right">
                                <input type="checkbox"
                                       class="custom-control-input"
                                       id="highlighted"
                                       name="highlighted"
                                       @if((isset($data) && $data["highlighted"] === "Si" || old("highlighted"))) checked @endif
                                >
                                <label class="custom-control-label" for="highlighted">
                                    {{__("articles.labels.highlighted")}}
                                </label>
                            </div>
                           
                            <div class="form-group">
                                <label for="lang" class="col-form-label">{{__("articles.labels.lang")}}</label>
                                <select class="form-control" name="lang" id="lang" required>
                                    <option @if(!(isset($data) && !$data["lang"] || !empty(old("lang")))) selected @endif>--
                                        Seleccione --
                                    </option>

                                    @foreach($language as $key => $lang)
                                        <option
                                            value="{{$key}}"
                                            @if((isset($data) && $data["lang"] === $lang || old("lang") === $key)) selected @endif
                                        >
                                            {{\Illuminate\Support\Str::title($lang)}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="main_image">{{__("articles.labels.image")}}</label>
                                <div>
                                    <div class="file-loading">
                                        <input id="main_image" name="image" type="file">
                                    </div>
                                </div>
                            </div>
                            
                           
                            <div class="dropdown-divider"></div>
                            <div class="form-group">
                                <button class="btn btn-primary float-right"
                                        id="save">{{isset($data) ? __("general.buttons.edit") :__("general.buttons.save")}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section("js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
@endsection
@section('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection
@section("scripts")
    <script type="application/javascript">
        $(document).ready(function() {
            if (!$("#publish").is(':checked')) {
                $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
            }
            $("#publish").change( function(){
                if( $(this).is(':checked') ) {
                    $("#datetime").removeAttr("readOnly").datepicker("option", "disabled", false);
                }else{
                    $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
                }
            });
            var now = moment();
            let date = $("#datetime").val().length < 0 ? $("#datetime").val() : now.format('DD/MM/YYYY');
            $('#datetime').datepicker({
                changeMonth: true,
                changeYear: true,
            });

            $.datepicker.regional['es'];
        });
        $(document).ready(function() {
            editor['data'] = {!! isset($data) ? $data["body"] : old("body", json_encode([])) !!};
            const saveButton = document.getElementById('save');
            saveButton.addEventListener('click', () => {
                editor.save().then(savedData => {
                    $("#articleBody").val(JSON.stringify(savedData, null, 4));
                })
            });
        })
    </script>
    <script type="application/javascript">

        @php
            $images = [];
            if(isset($data) && !empty($data["images"]) || old("images")) {
                $fileNames = isset($data) ? $data["images"] : old("images");
                foreach (json_decode($fileNames, true) as $fileName) {
                    $images[] = \Illuminate\Support\Facades\Storage::url("articles/" . $fileName);
                }
            }
        @endphp
        let functions = new Functions();
        $(document).ready(function () {
            $("#main_image").fileinput({
                @if(isset($data) || old("image"))
                    initialPreview: "{{ \Illuminate\Support\Facades\Storage::disk("public")->url("articles/".$data["image_url"]) }}",
                    initialPreviewAsData: true,
                    deleteUrl: '{{url("articles/deleteFile")}}',
                    initialPreviewConfig:[
                        {
                            key: "{{ $data["image_url"] }}",
                        },
                    ],
                    ajaxDeleteSettings: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    },
                @endif
                overwriteInitial: false,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                browseIcon: " <i class='fa fa-folder-open-o'></i>",
                browseLabel: '',
                removeLabel: '',
                removeIcon: '<i class="fa fa-trash"></i>',
                uploadUrl: '{{url("articles/uploadFile")}}',
                required: true,
                removeTitle: 'Cancel or reset changes',
                elErrorContainer: '#kv-avatar-errors-2',
                msgErrorClass: 'alert alert-block alert-danger',
                defaultPreviewContent: '<img src="{{asset("images/file-upload-image-icon.png")}}" class="image" alt="Subir imagen">',
                layoutTemplates: {main2: '{preview} {remove} {browse}'},
                allowedFileExtensions: ["jpg", "png", "gif", "jpeg",],
                ajaxSettings: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                previewZoomButtonIcons: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>',
                    toggleheader: '<i class="fa fa-arrows-h"></i>',
                    fullscreen: '<i class="fa fa-arrows-alt"></i>',
                    borderless: '<i class="fa fa-arrows"></i>',
                    close: '<i class="fa fa-times"></i>'
                },
                fileActionSettings: {
                    showUpload: false,
                    showRemove: false,
                    zoomIcon: "<i class='fa fa-search'></i>",
                    zoomTitle: "Ver imagen",
                    removeIcon: "<i class='fa fa-trash'></i>",
                    removeTitle: "Quitar imagen"
                }
            });
            $("#header_image_file").fileinput({
                @if(isset($data) || old("image"))
                    initialPreview: "{{ \Illuminate\Support\Facades\Storage::disk("public")->url("articles/".$data["image_url"]) }}",
                    initialPreviewAsData: true,
                    deleteUrl: '{{url("articles/deleteFile")}}',
                    initialPreviewConfig:[
                        {
                            key: "{{ $data["image_url"] }}",
                        },
                    ],
                    ajaxDeleteSettings: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    },
                @endif
                overwriteInitial: false,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                browseIcon: " <i class='fa fa-folder-open-o'></i>",
                browseLabel: '',
                removeLabel: '',
                removeIcon: '<i class="fa fa-trash"></i>',
                uploadUrl: '{{url("articles/uploadFile")}}',
                required: true,
                removeTitle: 'Cancel or reset changes',
                elErrorContainer: '#kv-avatar-errors-2',
                msgErrorClass: 'alert alert-block alert-danger',
                defaultPreviewContent: '<img src="{{asset("images/file-upload-image-icon.png")}}" class="image" alt="Subir imagen">',
                layoutTemplates: {main2: '{preview} {remove} {browse}'},
                allowedFileExtensions: ["jpg", "png", "gif", "jpeg",],
                ajaxSettings: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                previewZoomButtonIcons: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>',
                    toggleheader: '<i class="fa fa-arrows-h"></i>',
                    fullscreen: '<i class="fa fa-arrows-alt"></i>',
                    borderless: '<i class="fa fa-arrows"></i>',
                    close: '<i class="fa fa-times"></i>'
                },
                fileActionSettings: {
                    showUpload: false,
                    showRemove: false,
                    zoomIcon: "<i class='fa fa-search'></i>",
                    zoomTitle: "Ver imagen",
                    removeIcon: "<i class='fa fa-trash'></i>",
                    removeTitle: "Quitar imagen"
                }
            });
            $("#card_image_file").fileinput({
                @if(isset($data) || old("image"))
                    initialPreview: "{{ \Illuminate\Support\Facades\Storage::disk("public")->url("articles/".$data["image_url"]) }}",
                    initialPreviewAsData: true,
                    deleteUrl: '{{url("articles/deleteFile")}}',
                    initialPreviewConfig:[
                        {
                            key: "{{ $data["image_url"] }}",
                        },
                    ],
                    ajaxDeleteSettings: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    },
                @endif
                overwriteInitial: false,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                browseIcon: " <i class='fa fa-folder-open-o'></i>",
                browseLabel: '',
                removeLabel: '',
                removeIcon: '<i class="fa fa-trash"></i>',
                uploadUrl: '{{url("articles/uploadFile")}}',
                required: true,
                removeTitle: 'Cancel or reset changes',
                elErrorContainer: '#kv-avatar-errors-2',
                msgErrorClass: 'alert alert-block alert-danger',
                defaultPreviewContent: '<img src="{{asset("images/file-upload-image-icon.png")}}" class="image" alt="Subir imagen">',
                layoutTemplates: {main2: '{preview} {remove} {browse}'},
                allowedFileExtensions: ["jpg", "png", "gif", "jpeg",],
                ajaxSettings: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                previewZoomButtonIcons: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>',
                    toggleheader: '<i class="fa fa-arrows-h"></i>',
                    fullscreen: '<i class="fa fa-arrows-alt"></i>',
                    borderless: '<i class="fa fa-arrows"></i>',
                    close: '<i class="fa fa-times"></i>'
                },
                fileActionSettings: {
                    showUpload: false,
                    showRemove: false,
                    zoomIcon: "<i class='fa fa-search'></i>",
                    zoomTitle: "Ver imagen",
                    removeIcon: "<i class='fa fa-trash'></i>",
                    removeTitle: "Quitar imagen"
                }
            });
            let uploadImages = [];
            $("#file").fileinput({
                @if(isset($data) || old("images") && !empty($images))
                deleteUrl: '{{url("articles/deleteFile")}}',
                initialPreview: {!! json_encode($images) !!},
                initialPreviewAsData: true,
                overwriteInitial: false,
                initialPreviewConfig:[
                @foreach($images as $image)
                    {
                        key: "{{ basename($image) }}",
                    },
                @endforeach
                ],
                ajaxDeleteSettings: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                },
                @endif
                browseClass: "btn btn-primary",
                browseIcon: " <i class='fa fa-folder-open-o'></i>",
                browseLabel: "Buscar imagenes",
                showCaption: false,
                showRemove: false,
                showCancel: false,
                showUpload: true,
                uploadTitle: "Subir Imagen",
                uploadLabel: "Subir",
                allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                uploadUrl: '{{url("articles/uploadFile")}}',
                maxFilePreviewSize: 10240,
                maxFileCount: 10,
                required: true,
                validateInitialCount: true,
                showClose: false,
                ajaxSettings: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                previewZoomButtonIcons: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>',
                    toggleheader: '<i class="fa fa-arrows-h"></i>',
                    fullscreen: '<i class="fa fa-arrows-alt"></i>',
                    borderless: '<i class="fa fa-arrows"></i>',
                    close: '<i class="fa fa-times"></i>'
                },
                fileActionSettings: {
                    showUpload: false,
                    zoomIcon: "<i class='fa fa-search'></i>",
                    zoomTitle: "Ver imagen",
                    removeIcon: "<i class='fa fa-trash'></i>",
                    removeTitle: "Quitar imagen"
                }
            }).on('fileselect', function(event, numFiles, label) {
            }).on('fileremoved', function (event, id, index) {
                let image = $("#images");
                let value = JSON.parse(image.val());
                let fileName = index.split("_").splice(1);
                removeItemFromArr(value, fileName.join("_"));
                functions.request('{{url("articles/deleteFile")}}', 'POST', {
                    image: fileName[0]
                });
                image.val(JSON.stringify(fileName));
            }).on('fileuploaded', function (event, previewId, index, fileId) {
                let images = $("#images");
                let data = images.val().length > 0 ? JSON.parse(images.val()) : [];
                for (let i in previewId.files) {
                    let fileName = previewId.files[i].name.split(" ").join("_");
                    fileName = fileName.split("(").join("_");
                    fileName = fileName.split(")").join("_");
                    //fileName = fileName.split("-").join("_");
                    fileName = fileName.split(" ").join("_");
                    data.push(fileName);
                }
                images.val(JSON.stringify(Array.from(new Set(data))));
            })
            @if(isset($data) || old("images"))
            .on('filedeleted', function(event, key, jqXHR, data) {
                let image = $("#images");
                let value = JSON.parse(image.val());
                let fileName = key.split("_").splice(1);
                removeItemFromArr(value, fileName.join("_"));
                image.val(JSON.stringify(value));
            });
            @endif
        });

        function removeItemFromArr(arr, item) {
            let i = arr.indexOf( item );

            if ( i !== -1 ) {
                arr.splice( i, 1 );
            }
        }
    </script>
@endsection