<?php

return [
    "settings" => "Settings",
    "menu" => [
        "blog" => [
            "title" => "Blog",
            "sub-menu" => [
                "type" => "Type",
                "articles" => "Articles",
            ]
        ],
        "documents" => [
            "title" => "Documents",
            "sub-menu" => [
                "type" => "Type",
                "documents" => "Documents",
            ]
        ],

        "users" => [
            "title" => "Users",
        ],
    ]
];
