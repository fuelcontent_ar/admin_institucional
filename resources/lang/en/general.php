<?php

return [
    "buttons" => [
        "cancel" => "Cancel",
        "edit" => "Edit",
        "save" => "Save changes",
        "close" => "Close",
        "yes" => "Si",
        "no" => "No",
    ],
    "alert-type" => [
        "warning" => "!Warning!",
        "success" => "Success",
        "error" => "Error",
        "info" => "Information",
    ],
    "action" => "Action",
    "delete" => [
        "title" => "Removed",
        "message" => "Do you want to delete this record?",
    ],
    "unauthorized" => [
        "view" => "You are not authorized to read this resource.",
        "edit" => "You are not authorized to read this resource.",
        "delete" => "You are not authorized to delete this resource."
    ],
    "error_server" => "This resource is not available.",
    "validate" => [
        "error" => "You have to complete the data to continue."
    ],
    "boolean" => [
        "yes" => "Yes",
        "no" => "No",
    ]
];
