<?php

return [
    "all" => [
        "title" => "List of Blog",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/English.json",
        "tooltip-show" => "See blog data",
        "tooltip-edit" => "Edit blog data",
        "tooltip-destroy" => "Delete blog",
    ],
    "edit" => [
        "title" => "Edit blog title",
        "message" => "The data has been updated.",
    ],
    "save" => [
        "title" => "Create new blog title",
        "message" => "The blog title has been created.",
    ],
    "labels" => [
        "id" => "ID",
        "title" => "Title",
        "category" => "Categories",
        "articles_count" => "Quantity of Articles",
        "created_at" => "Created At",
        "user" => "Author",
    ],
    "controllers" => [
        "destroy" => "Blog title removed successfully.",
    ],
];
