<?php

return [
    "all" => [
        "title" => "list of document categories",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/English.json",
        "tooltip-show" => "See document category",
        "tooltip-edit" => "Edit the document category",
        "tooltip-destroy" => "Delete document category",
    ],
    "edit" => [
        "title" => "Edit the document category",
        "message" => "The data has been updated.",
    ],
    "save" => [
        "title" => "Create new document category",
        "message" => "The blog title has been created.",
    ],
    "labels" => [
        "id" => "ID",
        "title" => "Title",
        "created_at" => "Created At",
        "documents_count" => "Quantity of documents",
        "user" => "Author",
    ],
    "controllers" => [
        "destroy" => "Document title deleted successfully.",
    ],
];
