<?php

return [
    "all" => [
        "title" => "list of document",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/English.json",
        "tooltip-show" => "See document",
        "tooltip-edit" => "Edit the document",
        "tooltip-destroy" => "Delete document",
    ],
    "edit" => [
        "title" => "Edit the document",
        "button" => "Edit",
        "message" => "The data has been updated.",
    ],
    "save" => [
        "title" => "Create new document",
        "button" => "Create",
        "message" => "The blog title has been created.",
    ],
    "labels" => [
        "id" => "ID",
        "title" => "Title",
        "created_at" => "Created At",
        "user" => "Author",
        "document_type" => "Document category",
        "file" => "File",
        "input-file" => "Choose file",
    ],
    "controllers" => [
        "destroy" => "Document deleted successfully.",
    ],
];
