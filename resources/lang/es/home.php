<?php

return [
    "dashboard" => "Inicio",
    "welcome" => "Bienvenido",
    "settings" => "Configuraciones",
    "menu" => [
        "blog" => [
            "title" => "Blog",
            "sub-menu" => [
                "type" => "Tipo",
                "articles" => "Artículos",
            ]
        ],
        "documents" => [
            "title" => "Documentos",
            "sub-menu" => [
                "type" => "Tipo",
                "documents" => "Documentos",
            ]
        ],

        "users" => [
            "title" => "Usuarios",
        ],
    ]
];
