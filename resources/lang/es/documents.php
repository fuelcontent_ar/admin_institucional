<?php

return [
    "all" => [
        "title" => "Lista de documentos",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
        "tooltip-show" => "Ver documento",
        "tooltip-edit" => "Editar documento",
        "tooltip-destroy" => "Eliminar documento",
    ],
    "edit" => [
        "title" => "Editar documento",
        "button" => "Editar",
        "message" => "Los datos han sido actualizados.",
    ],
    "save" => [
        "title" => "Crear nuevo documento",
        "button" => "Crear",
        "message" => "Se ha creado el título del documento.",
    ],
    "labels" => [
        "id" => "ID",
        "title" => "Título",
        "created_at" => "Creado en",
        "user" => "Autor",
        "document_type" => "Categoría de documento",
        "file" => "Archivo",
        "input-file" => "Elija el archivo",
        "highlighted" => "Destacado",
        "lang" => "Lenguaje",
    ],
    "controllers" => [
        "destroy" => "Documento eliminado con éxito.",
    ],
];
