<?php

return [
    "buttons" => [
        "cancel" => "Cancelar",
        "save" => "Guardar cambios",
        "close" => "Cerrar",
        "edit" => "Editar",
        "yes" => "Si",
        "no" => "No",
    ],
    "alert-type" => [
        "warning" => "!Advertencia!",
        "success" => "Éxito",
        "error" => "Error",
        "info" => "Información",
    ],
    "action" => "Acciones",
    "delete" => [
        "title" => "Eliminado",
        "message" => "¿Quieres borrar este registro?",
    ],
    "unauthorized" => [
        "view" => "No estas autorizado(a) para leer este recurso.",
        "edit" => "No estas autorizado(a) para leer este recurso.",
        "delete" => "No estas autorizado(a) para eliminar este recurso."
    ],
    "error_server" => "Este recurso no esta disponible.",
    "validate" => [
        "error" => "Tienes que completar los datos para continuar."
    ],
    "boolean" => [
        "yes" => "Si",
        "no" => "No",
    ]
];
