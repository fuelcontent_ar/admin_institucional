<?php

return [
    "all" => [
        "title" => "Listado del configuraciones",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
        "tooltip-show" => "Ver datos de la configuracion",
        "tooltip-edit" => "Editar datos de la configuracion",
        "tooltip-destroy" => "Eliminar configuracion",
    ],
    "edit" => [
        "title" => "Editar configuracion",
        "button" => "Editar",
        "message" => "Los datos han sido actualizados.",
    ],
    "save" => [
        "title" => "Crear nueva configuracion",
        "button" => "Crear",
        "message" => "The configuration has been created.",
    ],
    "labels" => [
        "id" => "ID",
        "key" => "Identificador",
        "user" => "Creado por",
        "created_at" => "Creado en",
    ],
    "controllers" => [
        "destroy" => "Configuracion eliminado con éxito.",
    ],
];
