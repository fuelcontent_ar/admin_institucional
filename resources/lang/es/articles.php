<?php

return [
    "all" => [
        "title" => "Lista de artículos",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
        "tooltip-create" => "Crear artículo",
        "tooltip-show" => "Ver datos del artículos",
        "tooltip-edit" => "Editar datos del artículos",
        "tooltip-destroy" => "Eliminar artículos",
    ],
    "edit" => [
        "title" => "Editar artículo",
        "message" => "Los datos han sido actualizados.",
    ],
    "save" => [
        "title" => "Crear nuevo artículo",
        "message" => "Se ha creado el artículo.",
    ],
    "labels" => [
        "title" => "Título",
        "slug" => "URL",
        "blog" => "Blog",
        "category" => "Categorías",
        "published" => "Publicado",
        "published_at" => "Publicado en",
        "created_at" => "Creado en",
        "description" => "Descripcion",
        "body" => "Contenido",
        "image" => "Imagen principal",
        "highlighted" => "Destacado",
        "hasimageheader" => "Mostrar imagen en encabezado",
        "banner" => "Banner homepage",
        "gallery" => "Galeria de imagenes",
        "input-file" => "Elija el archivo",
        "images" => "Imagenes",
        "lang" => "Lenguaje",
        "video" => "Url video youtube",
    ],
    "controllers" => [
        "destroy" => "Artículo eliminado con éxito.",
    ],
    "option" => "Opciones de publicacion",
    "enable-disable" => "Publicar",
    "highlight" => "Destacar",
];
