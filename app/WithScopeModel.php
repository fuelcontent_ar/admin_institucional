<?php


namespace App;

use Illuminate\Database\Eloquent\Builder;

trait WithScopeModel
{
    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeToCreationDate(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereDate("create_at", "<=", $value);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeFromCreationDate(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereDate("create_at", ">=", $value);
    }
}
