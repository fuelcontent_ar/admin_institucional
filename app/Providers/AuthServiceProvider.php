<?php

namespace App\Providers;

use App\Article;
use App\Blog;
use App\Document;
use App\DocumentType;
use App\Policies\ArticlesPolicy;
use App\Policies\BlogPolicy;
use App\Policies\DocumentPolicy;
use App\Policies\DocumentTypePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Article::class => ArticlesPolicy::class,
        Document::class => DocumentPolicy::class,
        DocumentType::class => DocumentTypePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            return $user->hasRole('admin') ? true : null;
        });
    }
}
