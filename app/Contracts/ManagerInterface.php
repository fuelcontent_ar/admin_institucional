<?php


namespace App\Contracts;


use Illuminate\Database\Schema\Builder;

interface ManagerInterface
{
    /**
     * @return array
     */
    public function columns(): array;

    /**
     * @return string
     */
    public function dataTablesColumns(): string;

    /**
     * @return string
     */
    public function datatableConfig(): string;

    /**
     * @param $builder
     * @return mixed
     */
    public function datatable($builder);
}
