<?php


namespace App\Contracts;


interface RepositoryInterface
{
    /**
     * @param $model
     * @param array $inputs
     * @return mixed
     */
    public function update($model, array $inputs);

    /**
     * @param $model
     * @return mixed
     */
    public function destroy($model);

    /**
     * @param array $inputs
     * @return mixed
     */
    public function create(array $inputs);

    /**
     * @return mixed
     */
    public function all();
}
