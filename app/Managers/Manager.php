<?php


namespace App\Managers;

use Illuminate\Support\Str;
use App\Contracts\ManagerInterface;
use Yajra\DataTables\EloquentDataTable as Table;
use Yajra\DataTables\Facades\DataTables;

abstract class Manager implements ManagerInterface
{
    /**
     * @var array
     */
    protected $addColumns = [];

    /**
     * {@inheritDoc}
     */
    public function datatable($builder)
    {
        $datatable = DataTables::of($builder)
            ->addIndexColumn()
            ->addColumn('action', function($model){
                return $this->viewActionButtons($model);
            });
        return $this->addColumnsTable($datatable)
            ->rawColumns(array_merge(['action'], $this->addColumns))
            ->toJson();
    }

    /**
     * {@inheritDoc}
     */
    public function dataTablesColumns(): string
    {
        $column = [];
        foreach ($this->columns() as $item) {
            //bugfix for related models
            $name = $item;
            $data = $item;
            /*if(Str::contains($data, ".")){
                $data = Str::camel($data);
            }*/
            //
            $row = ["name" => $name, "data" => $data];
            if (in_array($item, $this->notOrder())) {
                $row["orderable"] = false;
            }
            if (in_array($item, $this->notFind())) {
                $row["searchable"] = false;
            }
            $column[] = $row;
        }
        return json_encode($column);
    }

    /**
     * @return array
     */
    abstract protected function notOrder(): array;

    /**
     * @return array
     */
    abstract protected function notFind(): array;

    /**
     * @param Table $dataTables
     * @return Table
     */
    abstract protected function addColumnsTable(Table $dataTables): Table;

    /**
     * @param $model
     * @return mixed
     */
    abstract protected function viewActionButtons($model);
}
