<?php


namespace App\Managers;


use Illuminate\Support\Facades\View;
use Yajra\DataTables\EloquentDataTable;

class BlogManager extends Manager
{

    /**
     * {@inheritDoc}
     */
    protected function notOrder(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    protected function notFind(): array
    {
        return ["articles_count"];
    }

    /**
     * {@inheritDoc}
     */
    protected function addColumnsTable(EloquentDataTable $dataTables): EloquentDataTable
    {
        return $dataTables;
    }

    /**
     * {@inheritDoc}
     */
    public function columns(): array
    {
        return [
            'title', 'articles_count', 'category.name', 'user.name', 'created_at'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function datatableConfig(): string
    {
        return json_encode([
            "style" => [
                ["targets" => 0, "width" => "35%",],
                ["targets" => 1, "className" => "text-center", "width" => "18%",],
                ["targets" => 4, "className" => "text-center",],
            ],
            "language" => [
                "url" => __("blogs.all.lang-datatables"),
            ]
        ]);
    }

    /**
     * {@inheritDoc}
     */
    protected function viewActionButtons($model)
    {
        return View::make("blog.button_action", ["model" => $model]);
    }
}
