<?php


namespace App\Managers;


use Illuminate\Support\Facades\View;
use Yajra\DataTables\EloquentDataTable;

class DocumentTypeManager extends Manager
{

    /**
     * {@inheritDoc}
     */
    protected function notOrder(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    protected function notFind(): array
    {
        return ["document_count", "created_at"];
    }

    /**
     * {@inheritDoc}
     */
    protected function addColumnsTable(EloquentDataTable $dataTables): EloquentDataTable
    {
        return $dataTables;
    }

    /**
     * {@inheritDoc}
     */
    public function columns(): array
    {
        return [
            'title', 'user.name', 'documents_count', "created_at"
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function datatableConfig(): string
    {
        return json_encode([
            "style" => [
                ["targets" => 0, "width" => "35%",],
                ["targets" => 1, "className" => "text-center",],
                ["targets" => 2, "className" => "text-center", "width" => "22%",],
            ],
            "language" => [
                "url" => __("document_type.all.lang-datatables"),
            ]
        ]);
    }

    /**
     * {@inheritDoc}
     */
    protected function viewActionButtons($model)
    {
        return View::make("document_type.button_action", ["model" => $model]);
    }
}
