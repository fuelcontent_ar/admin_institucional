<?php


namespace App\Managers;


use Illuminate\Support\Facades\View;
use Yajra\DataTables\EloquentDataTable;

class DocumentManager extends Manager
{

    /**
     * {@inheritDoc}
     */
    protected function notOrder(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    protected function notFind(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    protected function addColumnsTable(EloquentDataTable $dataTables): EloquentDataTable
    {
        return $dataTables;
    }

    /**
     * {@inheritDoc}
     */
    public function columns(): array
    {
        return [
            'title', 'document_type.title', "user.name", "lang", "highlighted", "created_at"
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function datatableConfig(): string
    {
        return json_encode([
            "style" => [
                ["targets" => 0, "width" => "35%",],
                ["targets" => 1, "className" => "text-center", "width" => "22%",],
                ["targets" => 2, "className" => "text-center",],
            ],
            "language" => [
                "url" => __("documents.all.lang-datatables"),
            ]
        ]);
    }

    /**
     * {@inheritDoc}
     */
    protected function viewActionButtons($model)
    {
        return View::make("documents.button_action", ["model" => $model]);
    }
}
