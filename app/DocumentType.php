<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    use WithScopeModel;

    /**
     * @var string[]
     */
    protected $fillable = ["title", "image_url", "user_id"];

    /**
     * @var string[]
     */
    protected $casts = [
        "created_at" => "datetime:d/m/Y"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeTitle(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("name", "LIKE", "%".$value."%");
    }
}
