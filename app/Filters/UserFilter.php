<?php

namespace App\Filters;

use App\Contracts\Models\FiltersInterface;

/**
 * Class UserFilters
 * @package App\Models\Filters
 */
class UserFilter extends Filters implements FiltersInterface
{
    /**
     * @var string[]
     */
    public $columnsFilter = [
        "active", "email", "name", "from_creation_date", "to_creation_date"
    ];
}
