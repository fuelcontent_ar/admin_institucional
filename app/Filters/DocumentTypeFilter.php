<?php

namespace App\Filters;

use App\Contracts\Models\FiltersInterface;

/**
 * Class DocumentTypeFilters
 * @package App\Models\Filters
 */
class DocumentTypeFilter extends Filters implements FiltersInterface
{
    /**
     * @var string[]
     */
    public $columnsFilter = [
        "title", "from_creation_date", "to_creation_date"
    ];
}
