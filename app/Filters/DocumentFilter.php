<?php

namespace App\Filters;

use App\Contracts\Models\FiltersInterface;

/**
 * Class DocumentFilters
 * @package App\Models\Filters
 */
class DocumentFilter extends Filters implements FiltersInterface
{
    /**
     * @var string[]
     */
    public $columnsFilter = [
        "title", "document_type", "user", "from_creation_date", "to_creation_date"
    ];
}
