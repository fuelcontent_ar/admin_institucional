<?php

namespace App\Filters;

use App\Contracts\Models\FiltersInterface;

/**
 * Class ArticleFilters
 * @package App\Models\Filters
 */
class ArticleFilter extends Filters implements FiltersInterface
{
    /**
     * @var string[]
     */
    public $columnsFilter = [
        "title",
        "lang",
        "author",
        "published",
        "description",
        "to_publish_date",
        "from_publish_date",
        "from_creation_date",
        "to_creation_date",
    ];
}
