<?php

namespace App\Filters;

use App\Contracts\Models\FiltersInterface;

/**
 * Class BlogFilters
 * @package App\Models\Filters
 */
class BlogFilter extends Filters implements FiltersInterface
{
    /**
     * @var string[]
     */
    public $columnsFilter = [
        "title", "category_id", "from_creation_date", "to_creation_date", "author",
    ];
}
