<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes, WithScopeModel;

    protected $fillable = [
        "title",
        "slug",
        "description",
        "body",
        "image_url",
        "published",
        "highlighted",
        "published_at",
        "user_id",
        "card_image",
        "header_image",
        "lang",
      
    ];

    protected $casts = [
        "created_at" => "datetime:d/m/Y",
        "published_at" => "datetime:d/m/Y",
        "published" => "boolean",
        "highlighted" => "boolean",
        "body" => "array",
        "images" => "array"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /*public function blog()
    {
        return $this->belongsTo(Blog::class);
    }*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $value
     */
    public function setPublishedAtAttribute($value)
    {
        $this->attributes["published_at"] = !empty($value)
            ? Carbon::createFromFormat("d/m/Y", $value)->format("Y-m-d")
            : null;
    }

    /**
     * @param $value
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getPublishedAttribute($value)
    {
        if (!empty($value)) {
            return $value ? __("general.boolean.yes") : __("general.boolean.no");
        }
    }

    /**
     * @param $value
     */
    public function setPublishedAttribute($value)
    {
        if (is_string($value)) {
            $this->attributes["published"] = $value === "on" ? 1 : 0;
        }
    }

    /**
     * @param $value
     */
    public function getHighlightedAttribute($value)
    {
        if (!empty($value)) {
            return $value ? __("general.boolean.yes") : __("general.boolean.no");
        }
    }
    /**
     * @param $value
     */
    public function setHighlightedAttribute($value)
    {
        if (is_string($value)) {
            $this->attributes["highlighted"] = $value === "on" ? 1 : 0;
        }
    }

    

    /**
     * @param $value
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getLangAttribute($value)
    {
        return config("general.language." . $value);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeTitle(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("name", "LIKE", "%" . $value . "%");
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    /*public function scopeBlog(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereHas("blog", function ($query) use ($value) {
            return $query->whereId(intval($value));
        });
    }*/

   

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopePublished(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->wherePublished($value);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeDescription(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("description", "LIKE", "%" . $value . "%");
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeToPublishDate(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereDate("published_at", "<=", $value);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeFromPublishDate(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereDate("published_at", ">=", $value);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeLang(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("lang", $value);
    }
}
