<?php

namespace App\Policies;

use App\Document;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Document  $document
     * @return mixed
     */
    public function view(User $user, Document $document)
    {
        return $user->isAdmin() || $user->id === $document->user_id
            ? Response::allow()
            : redirect()->back()->with([
                "error" => __("general.unauthorized.view")
            ]);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Document  $document
     * @return mixed
     */
    public function update(User $user, Document $document)
    {
        return $user->isAdmin() || $user->id === $document->user_id
            ? Response::allow()
            : Response::deny(__("general.unauthorized.edit"));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Document  $document
     * @return mixed
     */
    public function delete(User $user, Document $document)
    {
        return $user->isAdmin() || $user->id === $document->user_id
            ? Response::allow()
            : Response::deny(__("general.unauthorized.delete"));
    }
}
