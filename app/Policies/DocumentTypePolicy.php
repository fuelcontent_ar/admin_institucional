<?php

namespace App\Policies;

use App\DocumentType;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DocumentTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentType  $documentType
     * @return mixed
     */
    public function view(User $user, DocumentType $documentType)
    {
        return $user->isAdmin() || $user->id === $documentType->user_id
            ? Response::allow()
            : Response::deny(__("general.unauthorized.view"));
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentType  $documentType
     * @return mixed
     */
    public function update(User $user, DocumentType $documentType)
    {
        return $user->isAdmin() || $user->id === $documentType->user_id
            ? Response::allow()
            : Response::deny(__("general.unauthorized.edit"));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentType  $documentType
     * @return mixed
     */
    public function delete(User $user, DocumentType $documentType)
    {
        return $user->isAdmin() || $user->id === $documentType->user_id
            ? Response::allow()
            : Response::deny(__("general.unauthorized.delete"));
    }
}
