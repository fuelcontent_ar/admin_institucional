<?php

namespace App\View\Components;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class Table extends Component
{

    /**
     * @var array
     */
    private $columns;

    /**
     * @var array
     */
    private $dataTablesColumns;

    /**
     * @var string
     */
    private $route;

    /**
     * @var string
     */
    private $config;

    /**
     * @var string
     */
    private $name;


    /**
     * Create a new component instance.
     *
     * @param array $columns
     * @param string $dataTablesColumns
     * @param string $route
     * @param string $config
     * @param string $name
     */
    public function __construct(array $columns,  string $dataTablesColumns, string $route, string $config, string $name)
    {
        $this->columns = $columns;
        $this->dataTablesColumns = $dataTablesColumns;
        $this->route = $route;
        $this->config = $config;
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.table', [
            "route" => $this->route,
            "columns" => $this->extractColumnTitle(),
            "dataTablesColumns" => $this->dataTablesColumns,
            "config" => $this->config,
            "name" => $this->name,
        ]);
    }

    /**
     * @return array
     */
    private function extractColumnTitle()
    {
        foreach ($this->columns as $key => $column) {
            if (Str::containsAll($column, ["."])) {
                $this->columns[$key] = Str::of($column)->before(".");
            }
        }

        return $this->columns;
    }
}
