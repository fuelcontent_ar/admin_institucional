<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use WithScopeModel, SoftDeletes;

    /**
     * @var string[]
     */
    protected $fillable = ["title", "category_id", "user_id"];

    /**
     * @var string[]
     */
    protected $casts = [
        "created_at" => "datetime:d/m/Y"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeAuthor(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereHas("user", function ($query) use ($value) {
            return $query->whereId(intval($value));
        });
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeTitle(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("name", "LIKE", "%".$value."%");
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeCategoryId(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereHas("category", function ($query) use ($value) {
            return $query->whereId(intval($value));
        });
    }
}
