<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Document
 * @package App
 */
class Document extends Model
{
    use WithScopeModel;

    /**
     * @var string[]
     */
    protected $fillable = [
        "title",
        "image_url",
        "document_url",
        "highlighted",
        "document_type_id",
        "lang",
        "user_id"
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        "created_at" => "datetime:d/m/Y",
        "highlighted" => "boolean",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function documentType()
    {
        return $this->belongsTo(DocumentType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeTitle(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("name", "LIKE", "%".$value."%");
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeDocumentType(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereHas("documentType", function ($query) use ($value) {
            return $query->whereId(intval($value));
        });
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeUser(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereHas("user", function ($query) use ($value) {
            return $query->whereId(intval($value));
        });
    }

    /**
     * @param $value
     */
    public function getHighlightedAttribute($value)
    {
        if (!empty($value)) {
            return $value ? __("general.boolean.yes") : __("general.boolean.no");
        }
    }
    /**
     * @param $value
     */
    public function setHighlightedAttribute($value)
    {
        if (is_string($value)) {
            $this->attributes["highlighted"] = $value === "on" ? 1 : 0;
        }
    }

    /**
     * @param $value
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getLangAttribute($value)
    {
        return config("general.language." . $value);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeLang(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("lang", $value);
    }
}
