<?php


namespace App\Repositories;

use App\DocumentType;
use App\Filters\DocumentTypeFilter;
use App\Filters\Filters;

class DocumentTypeRepository extends Repository
{
    /**
     * {@inheritDoc}
     */
    protected function getModel()
    {
        if (!is_a($this->model, DocumentType::class)) {
            $this->model = app(DocumentType::class);
        }
        return $this->model;
    }

    /**
     * {@inheritDoc}
     */
    protected function getFilter(): Filters
    {
        if (!is_a($this->filter, DocumentTypeFilter::class)) {
            $this->filter = new DocumentTypeFilter($this->request);
        }

        return $this->filter;
    }

    /**
     * {@inheritDoc}
     */
    protected function withModel()
    {
        return $this->getModel()->query()
            ->with(["documents", "user"])
            ->withCount("documents");
    }

    /**
     * {@inheritDoc}
     */
    protected function extraData(): array
    {
        return [];
    }
}
