<?php


namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Filters\Filters;
use App\Pagination\Pagination;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class Repository implements RepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Filters
     */
    protected $filter;

    /**
     * @var Pagination
     */
    protected $page;

    /**
     * @var Request
     */
    protected $request;

    /**
     * Repository constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritDoc}
     */
    public function all()
    {
        return $this->getFilter()->apply($this->withModel());
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $inputs)
    {
        return $this->getModel()->create(array_merge($inputs, $this->extraData()));
    }

    /**
     * {@inheritDoc}
     */
    public function destroy($model)
    {
        $before = $model->toArray();
        $model->delete();
        return $before;
    }

    /**
     * {@inheritDoc}
     */
    public function update($model, array $inputs)
    {
        return $model->update($inputs);
    }

    /**
     * @return mixed
     */
    abstract protected function getModel();

    /**
     * @return Filters
     */
    abstract protected function getFilter(): Filters;

    /**
     * @return Builder
     */
    abstract protected function withModel();

    /**
     * @return array
     */
    abstract protected function extraData(): array;

}
