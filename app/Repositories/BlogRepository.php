<?php


namespace App\Repositories;

use App\Blog;
use App\Filters\BlogFilter;
use App\Filters\Filters;

class BlogRepository extends Repository
{
    /**
     * {@inheritDoc}
     */
    protected function getModel()
    {
        if (!is_a($this->model, Blog::class)) {
            $this->model = app(Blog::class);
        }
        return $this->model;
    }

    /**
     * {@inheritDoc}
     */
    protected function getFilter(): Filters
    {
        if (!is_a($this->filter, BlogFilter::class)) {
            $this->filter = new BlogFilter($this->request);
        }

        return $this->filter;
    }

    /**
     * {@inheritDoc}
     */
    protected function withModel()
    {
        return $this->getModel()->query()
            ->with(["category", "articles", "user"])
            ->withCount("articles");
    }

    /**
     * {@inheritDoc}
     */
    protected function extraData(): array
    {
        return [];
    }
}
