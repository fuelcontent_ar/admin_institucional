<?php


namespace App\Repositories;

use App\Document;
use App\Filters\DocumentFilter;
use App\Filters\Filters;

class DocumentRepository extends Repository
{
    /**
     * {@inheritDoc}
     */
    protected function getModel()
    {
        if (!is_a($this->model, Document::class)) {
            $this->model = app(Document::class);
        }
        return $this->model;
    }

    /**
     * {@inheritDoc}
     */
    protected function getFilter(): Filters
    {
        if (!is_a($this->filter, DocumentFilter::class)) {
            $this->filter = new DocumentFilter($this->request);
        }

        return $this->filter;
    }

    /**
     * {@inheritDoc}
     */
    protected function withModel()
    {
        return $this->getModel()->query()->with(["documentType", "user"]);
    }

    /**
     * {@inheritDoc}
     */
    protected function extraData(): array
    {
        return [];
    }
}
