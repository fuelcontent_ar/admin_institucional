<?php


namespace App\Repositories;

use App\Filters\Filters;
use App\Filters\UserFilter;
use App\User;

class UserRepository extends Repository
{
    /**
     * {@inheritDoc}
     */
    protected function getModel()
    {
        if (!is_a($this->model, User::class)) {
            $this->model = app(User::class);
        }
        return $this->model;
    }

    /**
     * {@inheritDoc}
     */
    protected function getFilter(): Filters
    {
        if (!is_a($this->filter, UserFilter::class)) {
            $this->filter = new UserFilter($this->request);
        }

        return $this->filter;
    }

    /**
     * {@inheritDoc}
     */
    protected function withModel()
    {
        return $this->getModel()->query();
    }

    /**
     * {@inheritDoc}
     */
    protected function extraData(): array
    {
        return ["password" => config("general.default-password")];
    }
}
