<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class AdministratorOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->isAdmin()) {
            abort(Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
