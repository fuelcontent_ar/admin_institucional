<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Permission\Models\Role;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "admin" => $this->isAdmin(),
            "active" => $this->active,
            "last_login" => $this->last_login,
            "rol" => $this->roles()->count() > 0 ? $this->roles()->first()->getAttribute("name") : null,
        ];
    }
}
