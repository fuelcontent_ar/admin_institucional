<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->getAttribute("id"),
            "title" => $this->getAttribute("title"),
            "created_at" => $this->getAttribute("created_at")->format("Y-m-d"),
            "category" => $this->category->getAttribute("name"),
            "category_id" => $this->category->getAttribute("id"),
        ];
    }
}
