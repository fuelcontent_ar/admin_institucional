<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->getAttribute("id"),
            "title" => $this->getAttribute("title"),
            "slug" => $this->getAttribute("slug"),
            "description" => $this->getAttribute("description"),
            "card_image" => $this->getAttribute("card_image"),
            "header_image" => $this->getAttribute("header_image"),
            "body" => $this->getAttribute("body"),
            "published" => $this->getAttribute("published"),
            "highlighted" => $this->getAttribute("highlighted"),
            "published_at" => !empty($this->getAttribute("published_at"))
                ? $this->getAttribute("published_at")->format("d/m/Y")
                : null,
            "user_id" => $this->user,
            "image_url" => $this->getAttribute("image_url"),
            "lang" => $this->getAttribute('lang'),
            
        ];
    }
}
