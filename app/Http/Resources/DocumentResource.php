<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "user" => $this->user->name,
            "highlighted" => $this->getAttribute("highlighted"),
            "lang" => $this->getAttribute('lang'),
            "image_url" => $this->getAttribute("image_url"),
            "document_type" => $this->documentType()->first(),
            "document_url" => $this->document_url,
        ];
    }
}
