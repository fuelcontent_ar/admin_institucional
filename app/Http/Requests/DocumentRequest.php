<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => ["required", "string"],
            "document_type_id" => ["required", "numeric", "exists:document_types,id"],
            "lang" => ["required", "string", "max:3"],
            "highlighted" => [Rule::in(['on', 'off']),],
            "image" => ["required", "image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=320,max_width=1920,min_height=380,max_height=1600"],
            "file" => [
                "required",
                "mimetypes:application/pdf",
                "max:30000",
                "file",
            ],
        ];
    }
}
