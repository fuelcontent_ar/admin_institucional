<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => ["required", "string", "max:255"],
            "category" => ["required", "numeric", "exists:categories,id"]
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            "title" => __("blogs.labels.title"),
            "category" => __("blogs.labels.category"),
        ];
    }
}
