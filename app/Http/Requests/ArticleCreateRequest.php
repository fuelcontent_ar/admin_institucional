<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ArticleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => ["required", "string", "max:100",],
            "slug" => ["required", "string", "max:100",],
            "description" => ["nullable","string"],
            "lang" => ["required", "string", "max:3"],
            "body" => ["required", "json",],
            "published" => [Rule::in(['on', 'off']),],
            "highlighted" => [Rule::in(['on', 'off']),],
            "image" => ["required", "image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=500,max_width=1920,min_height=500,max_height=1600"],
            "card_image" => [ "image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=500,max_width=1920,min_height=500,max_height=1600"],
            "header_image" => [ "image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=500,max_width=1920,min_height=500,max_height=1600"]
        ];
    }

    public function messages()
    {
        return [
            "image.required" => "La Imagen principal es requerida.",
            
        ];
    }

    public function attributes()
    {
        return [
                     
            "title" => __("articles.labels.title"),
            "description" => __("articles.labels.description"),
            "lang" => __("articles.labels.lang"),
            
        ];
    }
}
