<?php

namespace App\Http\Requests;

use App\Rules\EmptyArray;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ArticleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => ["string", "max:100",],
            "slug" => ["string", "max:100",],
            "description" => ["nullable","string",],
            "body" => [new EmptyArray, "json",],
            "card_image" => ["image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=500,max_width=1920,min_height=500,max_height=1600"],
            "header_image" => ["image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=500,max_width=1920,min_height=500,max_height=1600"],
            "lang" => ["string", "max:3"],
            "published" => [Rule::in(['on', 'off']),],
            "highlighted" => [Rule::in(['on', 'off']),],
            "image" => ["image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=500,max_width=1920,min_height=500,max_height=1600"],
        ];
    }

    public function messages()
    {
        return [
           
            "image.required" => "La Imagen principal es requerida.",
           
        ];
    }

    public function attributes()
    {
        return [
            "title" => __("articles.labels.title"),
            "description" => __("articles.labels.description"),
            "lang" => __("articles.labels.lang"),
            
        ];
    }
}
