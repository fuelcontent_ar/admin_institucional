<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "images_multiple" => ["required_without:image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=300,max_width=1920,min_height=300,max_height=1600"],
            "image" => ["required_without:images_multiple", "mimes:jpg,jpeg,png,svg", "max:900", "dimensions:min_width=20,max_width=1920,max_height=1600"],
        ];
    }
}
