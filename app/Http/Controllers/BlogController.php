<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Http\Requests\BlogRequest;
use App\Http\Resources\BlogResource;
use App\Managers\BlogManager;
use App\Repositories\BlogRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class BlogController extends Controller
{

    /**
     * @var Category
     */
    private $model;

    /**
     * Display a listing of the resource.
     *
     * @param BlogRepository $repository
     * @param BlogManager $manager
     * @return \Illuminate\Contracts\View\View
     */
    public function index(BlogRepository $repository, BlogManager $manager)
    {
        if (\request()->wantsJson()) {
            return $manager->datatable($repository->all());
        }
        return View::make("blog.all", [
            "columns" => $manager->columns(),
            "dataTablesColumns" => $manager->dataTablesColumns(),
            "route" => route("blogs.index"),
            "config" => $manager->datatableConfig(),
            "categories" => $this->getModel()->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BlogRequest $request
     * @param BlogRepository $repository
     * @return JsonResponse
     */
    public function store(BlogRequest $request, BlogRepository $repository)
    {
        $request->request->add(["category_id" => $request->input("category")]);
        $request->request->remove("category");
        $create = new BlogResource($repository->create(array_merge(
            $request->only(["title", "category_id"]),
            ["user_id" => auth()->user()->id]
        )));
        return response()->json([
            "message" => __("blogs.save.message"),
            "data" => $create->toArray($request)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Blog $blog
     * @return BlogResource
     */
    public function show(Blog $blog)
    {
        return new BlogResource($blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BlogRequest $request
     * @param \App\Blog $blog
     * @param BlogRepository $blogRepository
     * @return BlogResource
     */
    public function update(BlogRequest $request, Blog $blog, BlogRepository $blogRepository)
    {
        $request->request->add(["category_id" => $request->input("category")]);
        $request->request->remove("category");
        $blogRepository->update($blog, array_merge(
            $request->only(["title", "category_id"]),
            ["user_id" => auth()->user()->id]
        ));
        return new BlogResource($blog);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Blog $blog
     * @param BlogRepository $repository
     * @return JsonResponse
     */
    public function destroy(Blog $blog, BlogRepository $repository)
    {
        $blog->category()->dissociate();
        $repository->destroy($blog);
        return JsonResponse::fromJsonString(__("blogs.controllers.destroy"));
    }

    /**
     * @return Category
     */
    private function getModel()
    {
        if (!is_a($this->model, Category::class)) {
            $this->model = resolve(Category::class)->select("id", "name");
        }

        return $this->model;
    }
}
