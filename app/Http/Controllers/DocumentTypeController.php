<?php

namespace App\Http\Controllers;

use App\DocumentType;
use App\Http\Requests\DocumentTypeRequest;
use App\Http\Resources\DocumentTypeResource;
use App\Managers\DocumentTypeManager;
use App\Repositories\DocumentTypeRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\View;

class DocumentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param DocumentTypeRepository $repository
     * @param DocumentTypeManager $manager
     * @return \Illuminate\Contracts\View\View
     */
    public function index(DocumentTypeRepository $repository, DocumentTypeManager $manager)
    {
        if (\request()->wantsJson()) {
            return $manager->datatable($repository->all());
        }
        return View::make("document_type.all", [
            "columns" => $manager->columns(),
            "dataTablesColumns" => $manager->dataTablesColumns(),
            "route" => route("document-type.index"),
            "config" => $manager->datatableConfig(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DocumentTypeRequest $request
     * @param DocumentTypeRepository $repository
     * @return JsonResponse
     */
    public function store(DocumentTypeRequest $request, DocumentTypeRepository $repository)
    {
        $request->request->add(["user_id" => auth()->user()->getAuthIdentifier()]);
        $create = new DocumentTypeResource($repository->create($request->only("title", "image", "user_id")));
        return response()->json([
            "message" => __("document_type.save.message"),
            "data" => $create->toArray($request)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DocumentType  $documentType
     * @return DocumentTypeResource
     */
    public function show(DocumentType $documentType)
    {
        Gate::authorize('view', $documentType);
        return new DocumentTypeResource($documentType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DocumentTypeRequest $request
     * @param \App\DocumentType $documentType
     * @param DocumentTypeRepository $repository
     * @return DocumentTypeResource
     */
    public function update(DocumentTypeRequest $request, DocumentType $documentType, DocumentTypeRepository $repository)
    {
        Gate::authorize('update', $documentType);
        $repository->update($documentType, $request->only(["title", "image"]));
        return new DocumentTypeResource($documentType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\DocumentType $documentType
     * @param DocumentTypeRepository $repository
     * @return JsonResponse
     */
    public function destroy(DocumentType $documentType, DocumentTypeRepository $repository)
    {
        Gate::authorize('delete', $documentType);
        $repository->destroy($documentType);
        return JsonResponse::fromJsonString(__("document_type.controllers.destroy"));
    }
}
