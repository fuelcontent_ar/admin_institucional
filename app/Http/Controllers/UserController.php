<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Managers\UserManager;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    private $roles;

    public function __construct()
    {
        $this->middleware("administrator-only");
    }

    /**
     * Display a listing of the resource.
     *
     * @param UserRepository $repository
     * @param UserManager $manager
     * @return LengthAwarePaginator|\Illuminate\Contracts\View\View|Builder|Builder[]|Collection|string
     */
    public function index(UserRepository $repository, UserManager $manager)
    {
        if (\request()->wantsJson()) {
            return $manager->datatable($repository->all());
        }

        return View::make("users.all", [
            "columns" => $manager->columns(),
            "dataTablesColumns" => $manager->dataTablesColumns(),
            "route" => route("users.index"),
            "config" => $manager->datatableConfig(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return View::make("users.index", [
            "title" => "save",
            "roles" => $this->allRoles(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @param UserRepository $repository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request, UserRepository $repository)
    {
        $create = new UserResource($user = $repository->create($request->only(["name", "email", "active", "password"])));
        $user->assignRole($request->input("roles"));
        return redirect()->back()->with([
            "message" => __("users.save.message"),
            "data" => $create->toArray($request)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(User $user)
    {
        return View::make("users.index", [
            "data" => (new UserResource($user))->toArray(\request()),
            "title" => "edit",
            "roles" => $this->allRoles(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @param User $user
     * @param UserRepository $userRepository
     * @return UserResource|\Illuminate\Http\RedirectResponse
     */
    public function update(UserUpdateRequest $request, User $user, UserRepository $userRepository)
    {
        if ($request->has("roles") && !$user->hasRole($request->input("roles"))) {
            $user->assignRole($request->input("roles"));
        }

        $userRepository->update($user, $request->only(["name", "email", "active"]));
        if ($request->wantsJson()) {
            return new UserResource($user);
        }
        return redirect()->back()->with([
            "message" => __("users.edit.message"),
            "data" => (new UserResource($user))->toArray($request),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function destroy(User $user, UserRepository $userRepository)
    {
        foreach ($user->roles()->get() as $rol) {
            $user->removeRole($rol->name);
        }
        $userRepository->destroy($user);
        return JsonResponse::fromJsonString(__("users.controllers.destroy"));
    }

    /**
     * @return mixed
     */
    private function getRoles()
    {
        if (!is_a($this->roles, Role::class)) {
            $this->roles = resolve(Role::class);
        }
        return $this->roles;
    }

    /**
     * @return mixed
     */
    private function allRoles()
    {
        return $this->getRoles()->whereNotIn("name", ["admin"])->get();
    }
}
