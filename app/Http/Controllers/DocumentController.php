<?php

namespace App\Http\Controllers;

use App\Document;
use App\DocumentType;
use App\Http\Requests\DocumentRequest;
use App\Http\Requests\DocumentUpdateRequest;
use App\Http\Resources\DocumentResource;
use App\Http\Resources\DocumentTypeResource;
use App\Managers\DocumentManager;
use App\Repositories\DocumentRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class DocumentController extends Controller
{

    private $model;

    /**
     * Display a listing of the resource.
     *
     * @param DocumentRepository $repository
     * @param DocumentManager $manager
     * @return \Illuminate\Contracts\View\View
     */
    public function index(DocumentRepository $repository, DocumentManager $manager)
    {
        if (\request()->wantsJson()) {
            return $manager->datatable($repository->all());
        }
        
        return View::make("documents.all", [
            "columns" => $manager->columns(),
            "dataTablesColumns" => $manager->dataTablesColumns(),
            "route" => route("documents.index"),
            "config" => $manager->datatableConfig(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return View::make("documents.index", [
            "title" => "save",
            "language" => config('general.language'),
            "categories" => $this->allDocumentsType(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DocumentRequest $request
     * @param DocumentRepository $repository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DocumentRequest $request, DocumentRepository $repository)
    {
        $file = $request->file("file");
        $fileName = $file->getClientOriginalName();
        $file->storeAs("documents", $fileName, "public");
        $request->request->add([
            "user_id" => auth()->user()->getAuthIdentifier(),
            "document_url" => $fileName,
            "image_url" => $this->upload($request)
        ]);
        $this->renameFile($request);
        $create = new DocumentResource(
            $repository->create($request->only(
                "title",
                "user_id",
                "document_url",
                "document_type_id",
                "image_url",
                "lang",
                "highlighted"
            )
        ));
        return redirect()->back()->with([
            "message" => __("documents.save.message"),
            "data" => $create->toArray($request)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Document $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Document $document
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Document $document)
    {
        Gate::authorize('update', $document);
        return View::make("documents.index", [
            "title" => "edit",
            "categories" => $this->allDocumentsType(),
            "language" => config('general.language'),
            "data" => (new DocumentResource($document))->toArray(\request()),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DocumentUpdateRequest $request
     * @param \App\Document $document
     * @param DocumentRepository $repository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DocumentUpdateRequest $request, Document $document, DocumentRepository $repository)
    {
        Gate::authorize('update', $document);
        $this->uploadDocument($request, $document);
        $request->request->add([
            "image_url" => $request->hasFile("image") ? $this->upload($request) : $request->input("image_url"),
            "user_id" => auth()->user()->getAuthIdentifier(),
        ]);
        $this->renameFile($request);
        $repository->update($document, $request->only([
            "title",
            "document_url",
            "document_type_id",
            "image_url",
            "lang",
            "highlighted",
            "image_url"
        ]));
        return redirect()->back()->with([
            "message" => __("documents.edit.message"),
            "data" => (new DocumentResource($document))->toArray($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Document $document
     * @param DocumentRepository $repository
     * @return JsonResponse
     */
    public function destroy(Document $document, DocumentRepository $repository)
    {
        Gate::authorize('delete', $document);
        $repository->destroy($document);
        return JsonResponse::fromJsonString(__("documents.controllers.destroy"));
    }

    /**
     * @return mixed
     */
    private function getModel()
    {
        if (!is_a($this->model, DocumentType::class)) {
            $this->model = resolve(DocumentType::class);
        }

        return $this->model;
    }

    /**
     * @return mixed
     */
    private function allDocumentsType()
    {
        return $this->getModel()->select(["id", "title"])->get();
    }

    /**
     * @param Request $request
     * @param Document $document
     */
    private function uploadDocument(Request $request, Document $document)
    {
        if ($request->hasFile("file")) {
            $file = $request->file("file");
            $fileName = Str::uuid() . "." . $file->getClientOriginalExtension();
            if (Storage::disk("public")->exists("documents/" . $document->getAttribute("document_url"))) {
                Storage::disk("public")->delete("documents/" . $document->getAttribute("document_url"));
            }
            if (!Storage::disk("public")->exists("documents/" . $fileName)) {
                $file->storeAs("documents", $fileName, "public");
                $request->request->set("document_url", $fileName);
            }
        }
    }

    /**
     * @param Request $request
     * @return string|string[]
     */
    private function upload(Request $request)
    {
        $file = $request->file("image");
        $fileName = Str::uuid() . "." . $file->getClientOriginalExtension();
        $file->storeAs("articles", $fileName, "public");
        return $fileName;
    }

    /**
     * @param Request $request
     */
    private function renameFile(Request $request)
    {
        $path = "documents";
        if ($request->has("images") && !empty($request->input("images"))) {
            $images = [];
            foreach (json_decode($request->input("images"), true) as $image) {
                if (Storage::exists($path. "/". $image)) {
                    $extension = explode(".", $image);
                    $fileName = Str::uuid().".".end($extension);
                    Storage::move($path. "/". $image, $path. "/". $fileName);
                    $images[] = $fileName;
                }
            }
            $request->request->set("images", json_encode($images));
        }
    }
}
