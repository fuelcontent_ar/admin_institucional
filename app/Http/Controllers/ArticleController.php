<?php

namespace App\Http\Controllers;

use App\Article;
use App\Blog;
use App\Category;
use App\Http\Requests\ArticleCreateRequest;
use App\Http\Requests\ArticleUpdateRequest;
use App\Http\Requests\ArticleUploadRequest;
use App\Http\Resources\ArticleResource;
use App\Managers\ArticleManager;
use App\Repositories\ArticleRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class ArticleController extends Controller
{

    /**
     * @var Blog
     */
    private $model;

    /**
     * Display a listing of the resource.
     *
     * @param ArticleRepository $repository
     * @param ArticleManager $manager
     * @return \Illuminate\Contracts\View\View
     */
    public function index(ArticleRepository $repository, ArticleManager $manager)
    {
        if (\request()->wantsJson()) {
            return $manager->datatable($repository->all());
        }
        return View::make("articles.all", [
            "columns" => $manager->columns(),
            "dataTablesColumns" => $manager->dataTablesColumns(),
            "route" => route("articles.index"),
            "config" => $manager->datatableConfig(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return View::make("articles.show", [
            "title" => "save",
            "language" => config('general.language'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ArticleCreateRequest $request, ArticleRepository $repository)
    {
        if (!$request->has("published")) {
            $request->request->set("published_at", null);
        }

        
        $uploadedImages = $this->upload($request);

        $request->request->add([
            "user_id" => auth()->user()->getAuthIdentifier(),
            "image_url" => $uploadedImages['principal_image'],
            "card_image" => $uploadedImages['card_image'],
            "header_image" => $uploadedImages['header_image'],
            "published" => $request->has("published") ? "on" : "off",
            "highlighted" => $request->has("highlighted") ? "on" : "off",
        ]);
        //$this->renameFile($request);
        

        $create = new ArticleResource(
            $repository->create($request->only(
                "title",
                "slug",
                "description",
                "images",
                "lang",
                "body",
                "highlighted",
                "card_image",
                "header_image",
                "published",
                "published_at",
                "user_id",
                "image_url"
            )
        ));
        return redirect()->back()->with([
            "message" => __("articles.save.message"),
            "data" => $create->toArray($request)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Article $article
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Article $article)
    {
       
        Gate::authorize("view", $article);
        return View::make("articles.show", [
            "title" => "edit",
            "data" => (new ArticleResource($article))->toArray(\request()),
            "language" => config('general.language'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArticleUpdateRequest $request
     * @param \App\Article $article
     * @param ArticleRepository $repository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ArticleUpdateRequest $request, Article $article, ArticleRepository $repository)
    {
        Gate::authorize("update", $article);
        if (!$request->has("published")) {
            $request->request->set("published_at", null);
        }
        $request->request->add([
            "user_id" => auth()->user()->getAuthIdentifier(),
            "image_url" => $request->hasFile("image") ? $this->upload($request) : $request->input("image_url"),
            "highlighted" => $request->has("highlighted") ? "on" : "off",
            
            
        ]);
        $this->renameFile($request);
        
        $repository->update($article, $request->only(
            "title",
            "slug",
            "description",
            "images",
            "lang",
            "body",
            "published",
            "highlighted",
            "blog_id",
            "published_at",
            "user_id",
            "image_url"
        ));
        return redirect()->back()->with([
            "message" => __("articles.edit.message"),
            "data" => (new ArticleResource($article))->toArray($request)
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Article $article
     * @return JsonResponse
     */
    public function destroy(Article $article, ArticleRepository $repository)
    {
        Gate::authorize('delete', $article);
        $repository->destroy($article);
        return JsonResponse::fromJsonString(__("documents.controllers.destroy"));
    }

    /**
     * @param ArticleUploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadFile(ArticleUploadRequest $request)
    {
        $fileName = $this->upload($request);
        return response()->json([
            "type" => "image",
            "success" => 1,
            "file" => [
                "url" => Storage::url("articles/" . $fileName),
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFile(Request $request)
    {
        $fileName = $request->has("image") ? $request->input("image") : $request->input("key");
        if (Storage::exists("articles/".$fileName)) {
            Storage::delete("articles/".$fileName);
        }
        if ($request->has("key")) {
            return response()->json([
               $request->all()
            ]);
        }
    }

   

    /**
     * @param Request $request
     * @return string|string[]
     */
    private function upload(Request $request)
    {
        $cardImageName = "";
        $headerImageName = "";

        if ($request->hasFile("card_image_file")) {
            $card_image = $request->file("card_image_file");
            $cardImageName = Str::uuid() . "." . $card_image->getClientOriginalExtension();
            $card_image->storeAs("articles", $cardImageName, "public");       
        } 

        if ($request->hasFile("header_image_file")) {
            $header_image = $request->file("header_image_file");
            $headerImageName = Str::uuid() . "." . $header_image->getClientOriginalExtension();
            $header_image->storeAs("articles", $headerImageName, "public");     
        } 
            
        $principal_image = $request->file("image");
        $principalImageName = Str::uuid() . "." . $principal_image->getClientOriginalExtension();
        $principal_image->storeAs("articles", $principalImageName, "public");
        
        $fileNames = array(
            "card_image" => $cardImageName,
            "header_image" => $headerImageName,
            "principal_image" => $principalImageName
        );
        
        return $fileNames;
    }

    /**
     * @param Request $request
     */
    private function renameFile(Request $request)
    {
        $path = "articles";
        if ($request->has("images") && !empty($request->input("images"))) {
            $images = [];
            foreach (json_decode($request->input("images"), true) as $image) {
                if (Storage::exists($path. "/". $image)) {
                    $extension = explode(".", $image);
                    $fileName = Str::uuid().".".end($extension);
                    Storage::move($path. "/". $image, $path. "/". $fileName);
                    $images[] = $fileName;
                }
            }
            $request->request->set("images", json_encode($images));
        }
    }
}
